var default_sys_msg = "";

function check_uncheck_all(){
	var data_list = document.data_list;
	var checked = 1;
	var num_data = -1;
	var sys_msg = document.getElementById("sys_msg");

        if (default_sys_msg == "")
                default_sys_msg = sys_msg.innerHTML;

	for (var i=0; i<data_list.elements.length; i++){
		var element = data_list.elements[i];
		if (element.type == 'checkbox'){
			num_data++;
			if(data_list.master_box.checked == true){
				element.checked = false;
				checked--;
			} else {
				element.checked = true;
				checked++;
			}
		}
	}
	if(data_list.master_box.checked == true){
		data_list.master_box.checked = false;
		if(num_data == 0){
			sys_msg.innerHTML = data_0;
		} else {
			sys_msg.innerHTML = default_sys_msg;
		}
	} else { 
		data_list.master_box.checked = true;
		if(num_data == 0){
			sys_msg.innerHTML = data_0;
		} else if(checked == 1){
			sys_msg.innerHTML = data_1 +" на странице.";
		} else {
			sys_msg.innerHTML = "Выбрано " + checked + " из " + num_data + " " + data_more + " на странице.";
		}
	}
	data_list.num_checked.value = checked;
}

function check_uncheck_this(check_box){
	var data_list = document.data_list;
	var checked = 0;
	var num_data = -1;
	var sys_msg = document.getElementById("sys_msg");

	if (default_sys_msg == "")
		default_sys_msg = sys_msg.innerHTML;

	if(data_list.master_box.checked == true){
		data_list.master_box.checked = false;
	}
	for (var i=0; i<data_list.elements.length; i++){
		var element = data_list.elements[i];
		if (element.type == 'checkbox'){
			num_data++;
			if(element.checked == true){
				checked++;
			}
		}
	}
	if(num_data == 0){
		sys_msg.innerHTML = data_0;
	} else if(checked == 1){
		sys_msg.innerHTML = data_1 + " на странице.";
	} else if(checked == 0) {
		sys_msg.innerHTML = default_sys_msg;
	} else {
		sys_msg.innerHTML = "Выбрано " + checked + " из " + num_data +" " + data_more + " на странице.";
	}
	data_list.num_checked.value = checked;
}

function check_uncheck_row_checkbox(checkbox) {
	checkbox.checked = !checkbox.checked;
	check_uncheck_this(checkbox);
}

function undo_check_action(checkbox) {
	checkbox.checked = !checkbox.checked;
}
