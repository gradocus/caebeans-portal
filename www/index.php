<?php
// Show debug log (remove this line when in production mode).
define('WEB_APP_DEBUG', true);
define('WEB_APP_DEBUG_LEVEL', 3);

define('WEB_APP_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR."protected".DIRECTORY_SEPARATOR);

require_once WEB_APP_PATH."system/WebApp.php";

$webApp = new WebApp();
$webApp->run();
?>
