<?php
require_once WEB_APP_PATH."base/Controller.php";
//require_once WEB_APP_PATH."view/Error404Page.php";

class Error404 extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/(.+)\/?/' => "showError",
            ));
    }

    public function defaultAction()
    {
        header("HTTP/1.0 404 Not Found");

        $view = new Error404Page();
        $view->printPageSource();

        exit;
    }

    public function showError($request)
    {
        header("HTTP/1.0 404 Not Found");
        die("404 Page not found<br>$request[0]");
        $this->view = new View("errors/404", "main");
        $this->view['request'] = $request[0];
        $this->view->printPageSource();exit;
    }
}
?>
