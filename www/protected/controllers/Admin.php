<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."helpers/CAEServerClient.php";

class Admin extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/users\/?$/i' => "showUsers",
                '/testbeds\/?$/i' => "showTestbeds",
                '/testbeds\/server\/?$/i' => "getServerTestbedList",
                '/testbeds\/server\/add\/?$/i' => "addTestbedFromServer",
                '/users\/([0-9]+)\/?$/i' => "showUser",
                '/users\/([0-9]+)\/remove\/?$/i' => "removeUser",
                '/users\/([0-9]+)\/(allow|disallow)RequestForTestbed\/([0-9]+)\/?$/i' => "processRequest",
                '/testbeds\/([0-9]+)\/(hide|suspend|activate)\/?$/i' => "setTestbedState",
                '/testbeds\/([0-9]+)\/remove\/?$/i' => "removeTestbed",
                '/users\/([0-9]+)\/activate\/?$/i' => "activateUser",
                '/users\/([0-9]+)\/deactivate\/?$/i' => "deactivateUser",
            ));
    }

    protected function doActionProtection()
    {
        if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("account/signin/", true);
        }
        else
        {
            if (($userName = WebApp::$User->getEntity()->getName()) != "Administrator")
            {
                die("403");
            }
        }
    }

    public function defaultAction()
    {
        $this->view = new View("admin/home", "admin");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Панель управления";

        $this->view['requests'] = WebApp::$EntityManager->getRepository('RequestForTestbed')->findAll();
        $this->view['users'] = WebApp::$EntityManager->getRepository('User')->findBy(array('isBanned' => true));

        $this->view->printPageSource();
    }

    public function showUsers(Array $request)
    {
        $this->view = new View("admin/users", "admin_users");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Панель управления";

        $this->view['users'] = WebApp::$EntityManager->getRepository('User')->findBy(array(), array('firstName' => "asc"));

        $this->view->printPageSource();
    }

    public function showUser(Array $request = array())
    {
        $this->view = new View("admin/user", "admin_users");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Панель управления";

        $userId = (int)$request[0];
        $user = WebApp::$EntityManager->find('User', $userId);
        if (isset($_POST['submit']))
        {
            if ($user != WebApp::$User->getEntity())
                $user->setName(htmlspecialchars($_POST['group']));
            else
                unset($_POST['group']);
            $user->setLastName(htmlspecialchars($_POST['lastName']));
            $user->setFirstName(htmlspecialchars($_POST['firstName']));
            $user->setMiddleName(htmlspecialchars($_POST['middleName']));
            $user->setEmail(htmlspecialchars($_POST['email']));
            $user->setCompany(htmlspecialchars($_POST['company']));
            if (isset($_POST['newPassword1']) && isset($_POST['newPassword2'])
                   && $_POST['newPassword1'] === $_POST['newPassword2']
                   && $_POST['newPassword2'] !== "")
            {
                $user->setPassword(md5(md5($_POST['newPassword1'])));
            }
            WebApp::$EntityManager->flush();
        }

        $this->view['group'] = isset($_POST['group'])? htmlspecialchars($_POST['group']): $user->getName();
        $this->view['lastName'] = isset($_POST['lastName'])? htmlspecialchars($_POST['lastName']): $user->getLastName();
        $this->view['firstName'] = isset($_POST['firstName'])? htmlspecialchars($_POST['firstName']): $user->getFirstName();
        $this->view['middleName'] = isset($_POST['middleName'])? htmlspecialchars($_POST['middleName']): $user->getMiddleName();
        $this->view['email'] = isset($_POST['email'])? htmlspecialchars($_POST['email']): $user->getEmail();
        $this->view['company'] = isset($_POST['company'])? htmlspecialchars($_POST['company']): $user->getCompany();

        $this->view->printPageSource();
    }

    public function getServerTestbedList(Array $request)
    {
        $this->view = new View("json/view", "json");

        $testbeds = array();
        try
        {
            CAEServerClient::IndexAllProjects();
            $projectsIDs = CAEServerClient::GetProjectsIDs();
            if (!is_array($projectsIDs))
                $projectsIDs = array($projectsIDs);
            foreach ($projectsIDs as $projectID)
            {
                $problemCaebeans = CAEServerClient::GetProblemCaebeans($projectID);
                if (!is_array($problemCaebeans))
                    $problemCaebeans = array($problemCaebeans);
                foreach ($problemCaebeans as $caebeanID)
                {
                    if (!WebApp::$EntityManager->getRepository('Testbed')->testbedIsset($projectID, $caebeanID))
                    {
                    try {
                        $problemCaebeanXml = CAEServerClient::GetProblemCaebeanByID($projectID, $caebeanID);
                        if ($problemCaebeanXml == "")
                            continue;

                        $problemCaebeanDom = new DOMDocument();
                        $problemCaebeanDom->loadXML($problemCaebeanXml);
                        $problemCaebeans = $problemCaebeanDom->getElementsByTagName("problemCaebean");

                        $testbed = array();
                        $testbed['guid'] = $caebeanID;
                        $testbed['project'] = $projectID;
                        $testbed['name'] = $problemCaebeans->item(0)->getAttribute("name");
                        $testbed['author'] = $problemCaebeans->item(0)->getAttribute("author");
                        $testbed['version'] = $problemCaebeans->item(0)->getAttribute("version");

                        $testbeds[] = $testbed;
                    } catch (Exception $ex) { }
                    }
                }
            }
        } catch (Exception $ex) { }
        $this->view['testbeds'] = $testbeds;

        $this->view->printPageSource();
    }

    private function getLanguagesFromProblemCaebean($problemCaebean)
    {
        $languages = array();

        foreach ($problemCaebean->getElementsByTagName("language") as $language)
        {
            $languages[] = $language->getAttribute('xml:lang');
        }

        return $languages;
    }

    private function getLanguageFromProblemCaebean($problemCaebean, $lang)
    {
        $result = array();

        foreach ($problemCaebean->getElementsByTagName("language") as $language)
        {
            if ($language->getAttribute("xml:lang") != $lang)
                continue;

            foreach ($language->getElementsByTagName("data") as $data)
            {
                $result[$data->getAttribute("name")] = $data->nodeValue;
            }
        }

        return $result;
    }

    public function addTestbedFromServer(Array $request)
    {
        $projectId = $_POST['projectId'];
        $caebeanId = $_POST['caebeanId'];

        $problemCaebeanXml = CAEServerClient::GetProblemCaebeanByID($projectId, $caebeanId);
        $problemCaebeanDom = new DOMDocument();
        $problemCaebeanDom->loadXML($problemCaebeanXml);
        $problemCaebean = $problemCaebeanDom->getElementsByTagName("problemCaebean")->item(0);

        $languages = $this->getLanguagesFromProblemCaebean($problemCaebean);
        $language = isset($languages[0]) ? $this->getLanguageFromProblemCaebean($problemCaebean, $languages[0]) : array();

        $this->view = new View("json/view", "json");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setProjectGuid($projectId);
        $testbed->setGuid($caebeanId);
        $testbed->setName($problemCaebean->getAttribute("name"));
        $testbed->setAuthor($problemCaebean->getAttribute("author"));
        $testbed->setState("HIDDEN");
        foreach ($problemCaebean->getElementsByTagName("category") as $sCategory)
        {
            $category = new Category();
            $category->setName($sCategory->getAttribute("name"));
            $category->setTitle(isset($language[$sCategory->getAttribute("data")]) ? $language[$sCategory->getAttribute("data")] : $sCategory->getAttribute("data"));
            WebApp::$EntityManager->persist($category);

            foreach ($sCategory->getElementsByTagName("parameter") as $parameter)
            {
                $categoryParameter = new Parameter();
                WebApp::$EntityManager->persist($categoryParameter);
                $categoryParameter->setName($parameter->getAttribute("name"));
                $categoryParameter->setIsVisible($parameter->getAttribute("visible") == "true");
                $titleData = $parameter->getElementsByTagName("text")->item(0)->getAttribute("data");
                $categoryParameter->setTitle(isset($language[$titleData]) ? $language[$titleData] : $titleData);
                $categoryParameter->setType($parameter->getAttribute("type"));
                $unitsData = $parameter->getElementsByTagName("units")->item(0)->getAttribute("data");
                $categoryParameter->setUnits(isset($language[$unitsData]) ? $language[$unitsData] : $unitsData);
                $commentData = $parameter->getElementsByTagName("comment")->item(0)->getAttribute("data");
                $categoryParameter->setComment(isset($language[$commentData]) ? $language[$commentData] : $commentData);
                $categoryParameter->setValue(trim($parameter->getElementsByTagName("value")->item(0)->nodeValue));

                $constraints = $parameter->getElementsByTagName("constraints")->item(0);
                try {
                foreach ($constraints->getElementsByTagName("enum")->item(0)->getElementsByTagName("item") as $enumItem)
                {
                    $parameterEnum = new ParameterEnum();
                    WebApp::$EntityManager->persist($parameterEnum);
                    $parameterEnum->setValue($enumItem->nodeValue);
                    $categoryParameter->addParameterEnum($parameterEnum);
                }
                } catch(Exception $ex) { }

                $category->addParameter($categoryParameter);
            }

            $testbed->addCategory($category);
        }
        WebApp::$EntityManager->flush();
        $this->view['result'] = "success";
        $this->view['testbedId'] = $testbed->getId();

        $this->view->printPageSource();
    }

    public function setTestbedState(Array $request)
    {
        $this->view = new View("json/view", "json");

        $testbedId = (int)$request[0];
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        try
        {
            $action = $request[1];
            if ($action == "hide")
                $testbed->setState("HIDDEN");
            elseif ($action == "activate")
                $testbed->setState("AVAILABLE");
            elseif ($action == "suspend")
                $testbed->setState("SUSPENDED");
            else
                $this->view['result'] = "error";

            WebApp::$EntityManager->flush();
            $this->view['result'] = "success";
        }
        catch (Exception $ex)
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Ошибка обновления испытательного стенда.";
        }

        $this->view->printPageSource();
    }

    public function removeUser(Array $request)
    {
        $userId = (int)$request[0];
        $user = WebApp::$EntityManager->find('User', $userId);
        if ($user !== null && $user->getId() != WebApp::$User->getEntity()->getId())
        {
            WebApp::$EntityManager->remove($user);
            WebApp::$EntityManager->flush();
        }

        WebApp::$User->redirect("admin/");
    }

    public function processRequest(Array $request)
    {
        $userId = (int)$request[0];
        $action = $request[1];
        $testbedId = (int)$request[2];

        $user = WebApp::$EntityManager->find('User', $userId);
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        foreach ($user->getRequestsForTestbeds() as $request)
        {
            if ($request->getUser() == $user && $request->getTestbed() == $testbed)
            {
                if ($action == 'allow')
                {
                    $user->addTestbed($testbed);
                    WebApp::$EntityManager->remove($request);
                }
                else
                {
                    $user->getTestbeds()->removeElement($testbed);
                    WebApp::$EntityManager->remove($request);
                }
                WebApp::$EntityManager->flush();
            }
        }

        WebApp::$User->redirect("admin/");
    }

    public function removeTestbed(Array $request)
    {
        $this->view = new View("json/view", "json");

        $testbedId = (int)$request[0];
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        if (count($testbed->getJobs()) == 0)
        {
            foreach ($testbed->getCategories() as $category)
            {
                foreach ($category->getParameters() as $parameter)
                {
                    foreach ($parameter->getParameterEnums() as $parameterEnum)
                    {
                        WebApp::$EntityManager->remove($parameterEnum);
                    }
                    WebApp::$EntityManager->remove($parameter);
                }
                WebApp::$EntityManager->remove($category);
            }
            WebApp::$EntityManager->remove($testbed);
            WebApp::$EntityManager->flush();

            $this->view['result'] = "removed";
        }
        else
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Необходимо удалить все пользовательские задачи.";
        }

        $this->view->printPageSource();
    }

    public function activateUser(Array $request)
    {
        $this->view = new View("json/view", "json");

        $userId = (int)$request[0];
        $user = WebApp::$EntityManager->find('User', $userId);
        $user->setIsBanned(false);
        WebApp::$EntityManager->flush();

        $this->view['status'] = "activated";

        $this->view->printPageSource();
    }

    public function deactivateUser(Array $request)
    {
        $this->view = new View("json/view", "json");

        $userId = (int)$request[0];
        $user = WebApp::$EntityManager->find('User', $userId);
        if ($user !== null && $user->getId() != WebApp::$User->getEntity()->getId())
        {
            $user->setIsBanned(true);
            WebApp::$EntityManager->flush();

            $this->view['status'] = "deactivated";
        }
        else
        {
            $this->view['status'] = "error";
            $this->view['error'] = "Не возможно деактивировать себя.";
        }

        $this->view->printPageSource();
    }

    public function showTestbeds(Array $request)
    {
        $this->view = new View("admin/testbeds", "admin_testbeds");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Панель управления";

        $this->view['portalTestbeds'] = WebApp::$EntityManager->getRepository('Testbed')->findBy(array(), array('name' => "asc"));

        $testbeds = array();/*
        foreach (CAEServerClient::GetProjectsIDs() as $projectID)
        {
            try
            {
                $problemCaebean = CAEServerClient::GetProblemCaebean($projectID);
                $problemCaebean = $problemCaebean->problemCaebean;

                $testbed = new Testbed();
                $testbed->setName($problemCaebean->name);
                $testbed->setGuid($problemCaebean->caebeanId);
                $testbed->setAuthor($problemCaebean->author);
                $testbed->setVersion($problemCaebean->version);
/*
                foreach ($problemCaebean->categories as $category)
                {
                    var_dump($category);

                    if (!is_array($category->parameter))
                    {
                        var_dump($category->parameter);
                    }
                    else foreach ($category->parameter as $parameter)
                    {
                        var_dump($parameter);
                    }
                    
                }
*//*
                $testbeds[] = $testbed;
            }
            catch(Exception $ex) { }
        }*/
        $this->view['testbeds'] = $testbeds;

        $this->view->printPageSource();
    }
}
?>
