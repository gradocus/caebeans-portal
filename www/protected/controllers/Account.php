<?php
require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."models/LoginForm.php";
require_once WEB_APP_PATH."views/LoginPage.php";

class Account extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/^signin\/?$/i' => "signin",
                '/^settings\/?$/i' => "settings",
                '/^signup\/?$/i' => "signup",
                '/^signout\/?$/i' => "signout",
            ));
    }

    public function defaultAction()
    {
        if (!WebApp::$User->isAuthorised())
            $this->signin();
        else
            $this->settings();
    }

    public function signin(Array $request = array())
    {
        if (!WebApp::$User->isAuthorised())
        {
            $loginForm = new LoginForm();
            if (isset($_POST['LoginForm']))
            {
                $loginForm->login = htmlspecialchars($_POST['login']);
                $loginForm->password = $_POST['password'];
                $loginForm->rememberMe = isset($_POST['rememberMe']);
                if (WebApp::$User->login($loginForm->login, $loginForm->password, $loginForm->rememberMe))
                {
                    if (WebApp::$User->getEntity()->isBanned())
                    {
                        WebApp::$User->logout();
                        $loginForm->message = "Account isn't active.";
                    }
                    elseif (isset($_GET['go']))
                    {
                        header("Location: ".$_GET['go']);
                        exit;
                    }
                    else
                    {
                        header("Location: /");
                        exit;
                    }
                }
                else
                {
                    $loginForm->message = "Неверная пара логин/пароль.";
                }
            }
            $this->view = new View("auth/view", "account");
            $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Авторизация пользователя";

            $this->view['loginForm'] = $loginForm;

            $this->view->printPageSource();
        }
        else
        {
            throw new Exception("Login error");
        }
    }

    public function settings(Array $request = array())
    {
        $this->view = new View("account/settings");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Настройки пользователя";

        if (isset($_POST['submit']))
        {
            if (md5(md5($_POST['currentPassword'])) !== WebApp::$User->getEntity()->getPassword())
                $this->view['message'] = "Wrong current password";
            else
            {
                WebApp::$User->getEntity()->setLastName(htmlspecialchars($_POST['lastName']));
                WebApp::$User->getEntity()->setFirstName(htmlspecialchars($_POST['firstName']));
                WebApp::$User->getEntity()->setMiddleName(htmlspecialchars($_POST['middleName']));
                WebApp::$User->getEntity()->setEmail(htmlspecialchars($_POST['email']));
                WebApp::$User->getEntity()->setCompany(htmlspecialchars($_POST['company']));
                if (isset($_POST['newPassword1']) && isset($_POST['newPassword2'])
                       && $_POST['newPassword1'] === $_POST['newPassword2']
                       && $_POST['newPassword2'] !== "")
                {
                    WebApp::$User->getEntity()->setPassword(md5(md5($_POST['newPassword1'])));
                    if (isset($_COOKIE['password']))
                        setcookie('password', WebApp::$User->getEntity()->getPassword(), time() + 3600 * 24, '/');
                }
                WebApp::$EntityManager->flush();
            }
        }

        $this->view['lastName'] = isset($_POST['lastName'])? htmlspecialchars($_POST['lastName']): WebApp::$User->getEntity()->getLastName();
        $this->view['firstName'] = isset($_POST['firstName'])? htmlspecialchars($_POST['firstName']): WebApp::$User->getEntity()->getFirstName();
        $this->view['middleName'] = isset($_POST['middleName'])? htmlspecialchars($_POST['middleName']): WebApp::$User->getEntity()->getMiddleName();
        $this->view['email'] = isset($_POST['email'])? htmlspecialchars($_POST['email']): WebApp::$User->getEntity()->getEmail();
        $this->view['company'] = isset($_POST['company'])? htmlspecialchars($_POST['company']): WebApp::$User->getEntity()->getCompany();

        $this->view->printPageSource();
    }

    public function signup(Array $request = array())
    {
        $this->view = new View("account/signup", "account");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Регистрация нового пользователя";

        $this->view['lastName'] = isset($_POST['lastName'])? htmlspecialchars($_POST['lastName']): "";
        $this->view['firstName'] = isset($_POST['firstName'])? htmlspecialchars($_POST['firstName']): "";
        $this->view['middleName'] = isset($_POST['middleName'])? htmlspecialchars($_POST['middleName']): "";
        $this->view['email'] = isset($_POST['email'])? htmlspecialchars($_POST['email']): "";
        $this->view['company'] = isset($_POST['company'])? htmlspecialchars($_POST['company']): "";

        if (isset($_POST['password1']) && isset($_POST['password2'])
            && $_POST['password1'] == $_POST['password2'] && $_POST['password1'] != "")
        {
            $user = new User();
            $user->setName("User");
            $user->setLastName($this->view['lastName']);
            $user->setFirstName($this->view['firstName']);
            $user->setMiddleName($this->view['middleName']);
            $user->setEmail($this->view['email']);
            $user->setCompany($this->view['company']);
            $user->setPassword(md5(md5($_POST['password1'])));

            try
            {
                WebApp::$EntityManager->persist($user);
                WebApp::$EntityManager->flush();
                header("Location: /");
            }
            catch(Exception $ex)
            {
                $this->view['message'] = "Не возможно создать данный аккаунт.";
            }
        }

        $this->view->printPageSource();
    }

    public function signout(Array $request = array())
    {
        WebApp::$User->logout();

        if (isset($_GET['go']))
            header("Location: ".$_GET['go']);
        else
            header("Location: /");

        exit;
    }
}
?>
