<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."helpers/CAEServerClient.php";
require_once WEB_APP_PATH."helpers/CAEInstanceClient.php";
require_once WEB_APP_PATH."helpers/recursive_remove_directory.php";
require_once WEB_APP_PATH."helpers/zip_files_in_folder.php";

class Testbeds extends Controller
{
    public function __construct()
    {
        parent::__construct(array
            (
                '/^page_([0-9]+)\/?$/i' => "showTestbedsPage",
                '/^([0-9]+)\/?$/i' => "showTestbed",
                '/^([0-9]+)\/jobs\/?$/i' => "showJobs",
                '/^([0-9]+)\/request\/?$/i' => "requestForTestbed",
                '/^([0-9]+)\/request.json$/i' => "requestForTestbedJson",
                '/^([0-9]+)\/unsubscribe\/?$/i' => "unsubscribeFromTestbed",
                '/^([0-9]+)\/RemoveFile\/?$/i' => "removeFile",
                '/^([0-9]+)\/Submit\/?$/i' => "submitTestbed",
            ));
    }

    protected function doActionProtection()
    {
        if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("account/signin/", true);
        }
    }

    public function defaultAction()
    {
        $testbedsPerPage = WebApp::$Properties['application']['testbedsPerPage'];
        $this->showTestbedsPage(array(WebApp::$EntityManager->getRepository("Testbed")->getTestbedPageCount($testbedsPerPage)), false);
    }

    public function showTestbedsPage(array $request, $allButMyIsActive = true)
    {
        $this->view = new View("testbeds/list", "testbeds");

        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Испытательные стенды";
        $this->view['allButMyIsActive'] = $allButMyIsActive;

        $testbedsPerPage = WebApp::$Properties['application']['testbedsPerPage'];
        $this->view['testbed_page_count'] = WebApp::$EntityManager->getRepository("Testbed")->getTestbedPageCount($testbedsPerPage);
        if ($request[0] > $this->view['testbed_page_count'] || $request[0] == 0)
            $request[0] = $this->view['testbed_page_count'];
        $currentPage = 1 + $this->view['testbed_page_count'] - $request[0];
        $this->view['testbeds'] = WebApp::$EntityManager->getRepository("Testbed")->getTestbedsOnPage($currentPage, $testbedsPerPage);
        $this->view['page_number'] = $request[0];
        $this->view['myTestbeds'] = WebApp::$User->getEntity()->getTestbeds();
/*
        $testbeds = array();
        foreach ($this->view['testbeds'] as $testbed)
        {
            if (!$this->view['myTestbeds']->contains($testbed))
            {
                $testbeds[] = $testbed;
            }
        }
        $this->view['testbeds'] = $testbeds;
*/
        $this->view->printPageSource();
    }

    public function showTestbed(Array $request)
    {
        $testbedId = (int)$request[0];
        $user = WebApp::$User->GetEntity();
        $testbed = WebApp::$EntityManager->find("Testbed", $testbedId);
        if ($testbed === null || $testbed->getState() == "HIDDEN")
            die("404");

        $this->view = new View("testbeds/view", "testbeds");

        $this->view['testbedId'] = $testbedId;
        $this->view['testbed'] = $testbed;
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Испытательные стенды / ".$testbed->getName();

        $this->view->printPageSource();
    }

    public function showJobs(Array $request)
    {
        $testbedId = (int)$request[0];
        $user = WebApp::$User->GetEntity();
        $testbed = WebApp::$EntityManager->find("Testbed", $testbedId);
        if ($testbed === null || $testbed->getState() == "HIDDEN")
            die("404");

        $this->view = new View("testbeds/jobs", "testbed_jobs");
        $this->view['pageTitle'] = $user->GetName()." / Менеджер задач &mdash; ".WebApp::$Properties['application']['title'];

        $this->view['testbed'] = $testbed;
        $this->view['jobs'] = WebApp::$EntityManager->getRepository('Job')->getJobsOfUsersTestbed($user, $testbed);

        $this->view->printPageSource();
    }

    public function requestForTestbed(Array $request)
    {
        $testbedId = (int)$request[0];
        $user = WebApp::$User->getEntity();
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        if ($testbed === null)
        {
            die("404");
        }
        elseif ( $testbed->getState() != "AVAILABLE")
        {
            die("403");
        }
        elseif ($user->getTestbeds()->contains($testbed))
        {
            die("500");
        }

        try
        {
            $requestForTestbed = new RequestForTestbed();
            $requestForTestbed->setUser($user);
            $requestForTestbed->setTestbed($testbed);
            WebApp::$EntityManager->persist($requestForTestbed);

            WebApp::$EntityManager->flush();

            WebApp::$User->redirect("testbeds/");
        }
        catch (Exception $ex)
        {
            die("500");
        }
    }

    public function requestForTestbedJson(Array $request)
    {
        $testbedId = (int)$request[0];
        $user = WebApp::$User->getEntity();
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        $this->view = new View("json/view", "json");

        if ($testbed === null)
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Testbed not found";
        }
        elseif ( $testbed->getState() != "AVAILABLE")
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Forbidden";
        }
        elseif ($user->getTestbeds()->contains($testbed))
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Internal error";
        }

        try
        {
            $requestForTestbed = new RequestForTestbed();
            $requestForTestbed->setUser($user);
            $requestForTestbed->setTestbed($testbed);
            WebApp::$EntityManager->persist($requestForTestbed);

            WebApp::$EntityManager->flush();

            $this->view['result'] = "success";
            $this->view['error'] = "";
        }
        catch (Exception $ex)
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Already requested";
        }

        $this->view->printPageSource();
    }

    public function unsubscribeFromTestbed(Array $request)
    {
        $testbedId = (int)$request[0];
        $user = WebApp::$User->getEntity();
        $testbed = WebApp::$EntityManager->find('Testbed', $testbedId);

        if ($testbed === null || $testbed->getState() == "HIDDEN")
            die("404");

        $jobCount = WebApp::$EntityManager->getRepository('Job')->getJobCountOfUsersTestbed($user, $testbed);

        $this->view = new View("json/view", "json");

        if ($testbed !== null)
        {
            if ($jobCount > 0)
            {
                $this->view['result'] = "error";
                $this->view['error'] = "Необходимо удалить все задачи данного стенда.";
            }
            else
            {
                $user->getTestbeds()->removeElement($testbed);
                $testbed->getUsers()->removeElement($user);
                WebApp::$EntityManager->flush();

                $this->view['result'] = "success";
                $this->view['error'] = "";
            }
        }
        else
        {
            $this->view['result'] = "error";
            $this->view['error'] = "Testbed not found";
        }

        $this->view->printPageSource();
    }

    public function removeFile(Array $request)
    {
        $testbedId = (int)$request[0];
        $testbed = WebApp::$EntityManager->find("Testbed", $testbedId);
        if ($testbed === null || $testbed->getState() == "HIDDEN")
            die("404");
        $user = WebApp::$User->getEntity();
        if (!$user->getTestbeds()->contains($testbed))
            die("403");

        $this->view = new View("json/view", "json");

        if (!preg_match("/^[a-zA-Z0-9-_\.]+$/", $_POST["fileName"]))
        {
            $this->view['status'] = "Error";
            $this->view['error'] = "Invalid filename.";
            return;
        }

        $uploadedFile = WebApp::$EntityManager->find("UploadedFile", array('user' => $user->getId(), 'testbed' => $testbed->getId()));
        WebApp::$EntityManager->remove($uploadedFile);
        WebApp::$EntityManager->flush();

        $fileDir = WEB_APP_PATH."uploadedFiles/".$user->getId()."_".$testbedId;
        if (file_exists($fileDir."/".$_POST["fileName"]) && !is_dir($fileDir."/".$_POST["fileName"]))
        {
            unlink($fileDir."/".$_POST["fileName"]);
            $this->view['status'] = "Removed";
        }
        else
        {
            $this->view['status'] = "Error";
            $this->view['error'] = "File not found";
        }

        $this->view->printPageSource();
    }

    public function submitTestbed(Array $request)
    {
        $testbedId = (int)$request[0];
        $testbed = WebApp::$EntityManager->find("Testbed", $testbedId);
        if ($testbed === null)
            die("404");
        elseif ($testbed->getState() != "AVAILABLE")
            die("403");
        $user = WebApp::$User->getEntity();
        if (!$user->getTestbeds()->contains($testbed))
            die("403");

        $job = new Job();
        WebApp::$EntityManager->persist($job);

        $sourceDir = WEB_APP_PATH."uploadedFiles/".WebApp::$User->getEntity()->getId()."_".$testbedId;
        $mustCreateZip = file_exists($sourceDir) && is_dir($sourceDir) && !file_exists($sourceDir.".zip");
        if ($mustCreateZip && zip_files_in_folder($sourceDir))
        {
            recursive_remove_directory($sourceDir);
        }

        $instanceGuid = CAEServerClient::CreateInstance($testbed->getProjectGuid());
        $job->setGuid($instanceGuid);
        $job->setName($_POST['user_task_name']);
        $job->setTestbed($testbed);
        $job->setUser($user);
        $startTime = new DateTime();
        $job->setStartTime($startTime);
        $completeTime = new DateTime();
        $completeTime->sub(new DateInterval("PT5M"));
        $job->setCompleteTime($completeTime);

        WebApp::$EntityManager->flush();

        foreach ($testbed->getCategories() as $category)
        {
            foreach ($category->GetParameters() as $parameter)
            {
                $parameterKey = $category->getName()."_".$parameter->getName();
                if (isset($_POST[$parameterKey]) && $_POST[$parameterKey] !== $parameter->getValue())
                {
                    $parameterValue = new ParameterValue();
                    $parameterValue->setJob($job);
                    $parameterValue->setParameter($parameter);
                    $parameterValue->setValue($_POST[$parameterKey]);

                    WebApp::$EntityManager->persist($parameterValue);
                }
            }
        }

        if (file_exists($sourceDir.".zip"))
        {
            CAEInstanceClient::UploadUpdates($instanceGuid, file_get_contents($sourceDir.".zip"));
            unlink($sourceDir.".zip");
        }//else echo "Unuploaded<br>";

        try
        {
            CAEInstanceClient::SubmitJob($job);
            WebApp::$EntityManager->flush();
        }
        catch (Exception $ex) { } // 404 on the server.

        WebApp::$User->redirect("jobs/");
    }
}
?>
