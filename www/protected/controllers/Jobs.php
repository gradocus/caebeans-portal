<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."views/JobsPage.php";

require_once WEB_APP_PATH."helpers/CAEInstanceClient.php";
require_once WEB_APP_PATH."helpers/curl_remote_file_size.php";

require_once WEB_APP_PATH."system/errors/Error403.php";
require_once WEB_APP_PATH."system/errors/Error404.php";

class Jobs extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/^Page_([0-9]+)\/?$/i' => "showJobsPage",
                '/^([0-9]+)\/?$/i' => "showJobData",
                '/^([0-9]+)\/update_state\/?$/i' => "updateJobState",
//                '/^([0-9]+)\/restart\/?$/i' => "restartJob",
                '/^([0-9]+)\/results\/?$/i' => "getJobResults",
                '/^([0-9]+)\/works\/?$/i' => "getJobResults",
                '/^([0-9]+)\/remove\/?$/i' => "removeJob",
                '/^([0-9]+)\/stop\/?$/i' => "stopJob",
            ));
    }

    protected function doActionProtection()
    {
        if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("account/signin/", true);
        }
    }

    public function defaultAction()
    {
        // Show the first jobs page.
        $jobsPerPage = 8;
        $this->showJobsPage(array(WebApp::$EntityManager->getRepository("Job")->getJobPageCount($jobsPerPage)));
    }

    public function showJobsPage(Array $request)
    {
        $this->view = new View("jobs/list", "jobs");
        $this->view['pageTitle'] = WebApp::$User->GetEntity()->GetName()." / Менеджер задач &mdash; ".WebApp::$Properties['application']['title'];

        $jobsPerPage = 8;
        $this->view['job_page_count'] = WebApp::$EntityManager->getRepository("Job")->getJobPageCount($jobsPerPage);
        if ($request[0] > $this->view['job_page_count'] || $request[0] == 0)
            $request[0] = $this->view['job_page_count'];
        $currentPage = 1 + $this->view['job_page_count'] - $request[0];
        $this->view['jobs'] = WebApp::$EntityManager->getRepository("Job")->getJobsOnPage($currentPage, $jobsPerPage);
        $this->view['page_number'] = $request[0];

        $this->view->printPageSource();
    }

    public function showJobData(Array $request)
    {
        $this->view = new View("jobs/view", "jobs");

        $this->view['jobId'] = (int)$request[0];
        $this->view['job'] = WebApp::$EntityManager->find("Job", $this->view['jobId']);
        if ($this->view['job'] === null)
            throw new Error404();

        $user = WebApp::$User->getEntity();
        if ($this->view['job']->getUser()->getId() != $user->getId())
            throw new Error403();

        $this->view['parameterValues'] = $this->view['job']->getParameterValues();
        $this->view['testbed'] = $this->view['job']->getTestbed();
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Задачи / ".$this->view['job']->getName();

        $this->view->printPageSource();
    }

    public function updateJobState(Array $request = array())
    {
        $this->view = new View("json/view", "json");

        $jobId = (int)$request[0];
        $job = WebApp::$EntityManager->find("Job", $jobId);
        if ($job === null)
            throw new Error404();

        $user = WebApp::$User->getEntity();
        if ($job->getUser()->getId() != $user->getId())
            throw new Error403();

        if (in_array($job->getState(), array('NOT_STARTED', 'RUNNING')))
        {
            $state = CAEInstanceClient::GetStatus($job->getGuid());
            if ($job->getState() != $state)
            {
                $job->setState($state);
                $execTimestamp = CAEInstanceClient::GetExecTime($job->getGuid());
                if ($execTimestamp != -1)
                {
                    $date = new DateTime();
                    $date->setTimestamp($job->getStartTime()->getTimestamp() + $execTimestamp);
                    $job->setCompleteTime($date);
                }
                WebApp::$EntityManager->flush();

                if ($state != 'RUNNING')
                {
                    $this->view['status'] = "Updated";
                    $this->view['state'] = $state;
                    $this->view['completeTime'] = $job->getCompleteTime()->format('Y-m-d<\b\r>H:i:s');
                }
            }
            if (!isset($this->view['status']))
            {
                $this->view['status'] = "NotChanged";
            }
        }
        elseif ($job !== null)
        {
            $this->view['status'] = "Updated";
            $this->view['state'] = $job->getState();
            $this->view['completeTime'] = $job->getCompleteTime()->format('Y-m-d<\b\r>H:i:s');
        }
        else
        {
            $this->view['status'] = "Error";
            $this->view['error'] = "Job not found.";
        }

        $this->view->printPageSource();
    }

    public function getJobResults(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);

        if ($job === null)
            throw new Error404();

        $user = WebApp::$User->getEntity();
        if ($job->getUser()->getId() != $user->getId())
            throw new Error403();

        $workArchName = $job->getGuid().".zip";
        $workArchPath = "http://".WebApp::$Properties['caeserver']['address'].":".WebApp::$Properties['caeserver']['adminPort']."/results/".$workArchName;
        if ($workArch = fopen($workArchPath, "r"))
        {
            header("Content-Type: application/zip");
            header("Content-Disposition: attachment; filename=".$workArchName);
            
            $workArchSize = curl_remote_file_size($workArchPath);
            if ($workArchSize > 0 && $workArchSize < 2147483648)
            {
                header("Content-length: ".$workArchSize);
            }
            while(!connection_aborted() && !feof($workArch))
            {
                echo fread($workArch, 1024);
                flush();
            }
            fclose($workArch);
        }
        else
        {
            header($_SERVER['SERVER_PROTOCOL']." 404 Not Found");
            header("Status: 404 Not Found");
        }
        exit;
    }

    public function stopJob(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);
        if ($job === null)
            throw new Error404();

        $user = WebApp::$User->getEntity();
        if ($job->getUser()->getId() != $user->getId())
            throw new Error403();

        if (in_array($job->getState(), array('NOT_STARTED', 'RUNNING')))
        {
            // TODO: Add stopping job request.
            $job->setState('HELD');
            $date = new DateTime();
            $job->setCompleteTime($date);
            WebApp::$EntityManager->flush();
        }
        header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    public function removeJob(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);
        if ($job === null)
            throw new Error404();

        $user = WebApp::$User->getEntity();
        if ($job->getUser()->getId() != $user->getId())
            throw new Error403();

        if ($job->getState() !== 'RUNNING')
        {
            foreach ($job->getParameterValues() as $parameterValue)
                WebApp::$EntityManager->remove($parameterValue);
            if ($job->getState() === 'SUCCESSFULL')
                CAEInstanceClient::RemoveResultDir($job->getGuid());
            WebApp::$EntityManager->remove($job);
            WebApp::$EntityManager->flush();

            header("Location: ".$_SERVER['HTTP_REFERER']);
        } else {
            WebApp::$User->redirect("Jobs?message=error");
        }
    }
}
?>
