<?php
require_once WEB_APP_PATH."base/Controller.php";

class Home extends Controller
{
    public function __construct()
    {
        parent::__construct(array());
    }

    public function defaultAction()
    {
        if (WebApp::$User->IsAuthorised())
            WebApp::$User->redirect("jobs/");
        else
            WebApp::$User->redirect("account/signin/");
    }
}
?>
