<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."helpers/CAEServerClient.php";

class Install extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/ls-dyna-test\/?/i' => "LsDynaTest",
                '/iteration\/?/i' => "iteration",
                '/sleep\/?/i' => "sleep",
                '/fcp\/?/i' => "installFCP",
                '/big_file\/?/i' => "bigFile",
            ));
    }

    public function defaultAction()
    {
        $group = new Group();
        $group->setName("Administrator");

        $user = new User();
        $user->setName("Administrator");
        $user->setIsBanned(false);
        $user->setFirstName("Администратор");
        $user->setMiddleName("портала");
        $user->setEmail("admin@example.com");
        $user->setCompany("South Ural State University");
        $user->setPassword(md5(md5("admin4caeportal")));
        $user->addGroup($group);

        WebApp::$EntityManager->persist($user);
        WebApp::$EntityManager->persist($group);

        WebApp::$EntityManager->flush();

        echo "new User's id: ", $user->getId(), "<br><br>\n";
        $users = WebApp::$EntityManager->getRepository('User')->getRecentUsers(5);
        foreach($users as $user)
        {
            echo $user->getId(), " - ", $user->getName(), " (", $user->getEmail(), ")<br>\n";
            foreach($user->getGroups() as $group)
            {
                echo " - ", $group->getName(), "<br>\n";
            }
        }

        echo "<br>Administrator:<br>\n";
        $admin = WebApp::$EntityManager->getRepository('User')->findOneBy(array('name' =>"Administrator"));
        echo $admin->getId(), " - ", $admin->getName(), " (", $admin->getEmail(), ")<br>\n";
        foreach($admin->getGroups() as $group)
        {
            echo " - ", $group->getName(), "<br>\n";
        }
    }

    public function iteration(Array $request)
    {
        self::removeExistingTestbedWithGuid("iteration");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("iteration");
        $testbed->setName("iteration");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("internal");
        $parameterCategory->setTitle("internal");

        $param = new Parameter();
        WebApp::$EntityManager->persist($param);
        $param->setName("IterCount");
        $param->setTitle("");
        $param->setType("Integer");
        $param->setUnits("");
        $param->setComment("");
        $param->setValue(5);
        $parameterCategory->addParameter($param);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    public function sleep(Array $request)
    {
        CAEServerClient::IndexAllProjects();

        $this->setupSleep();

        for ($i = 1; $i < 10; $i++)
        {
            $this->setupSleep("sleep".$i);
        }
    }

    protected function setupSleep($sleepName = "sleep")
    {
        self::removeExistingTestbedWithGuid($sleepName);

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid($sleepName);
        $testbed->setName($sleepName);
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("internal");
        $parameterCategory->setTitle("internal");

        $param = new Parameter();
        WebApp::$EntityManager->persist($param);
        $param->setName("LengthInMegabytes");
        $param->setTitle("Время");
        $param->setType("Integer");
        $param->setUnits("сек");
        $param->setComment("Время сна в секундах.");
        $param->setValue(60);
        $parameterCategory->addParameter($param);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    public function bigFile(Array $request)
    {
        self::removeExistingTestbedWithGuid("big_file");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("big_file");
        $testbed->setName("big_file");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("internal");
        $parameterCategory->setTitle("internal");

        $param = new Parameter();
        WebApp::$EntityManager->persist($param);
        $param->setName("LengthInMegabytes");
        $param->setTitle("");
        $param->setType("Integer");
        $param->setUnits("");
        $param->setComment("");
        $param->setValue(200);
        $parameterCategory->addParameter($param);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    public function LsDynaTest(Array $request)
    {
        self::removeExistingTestbedWithGuid("ls-dyna-test");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("ls-dyna-test");
        $testbed->setName("ls-dyna-test");
        $testbed->setAuthor("Mr. Smith");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("internal");
        $parameterCategory->setTitle("Параметры");

        $vx = new Parameter();
        WebApp::$EntityManager->persist($vx);
        $vx->setName("temp1");
        $vx->setTitle("Vx");
        $vx->setType("Float");
        $vx->setUnits("м/с");
        $vx->setComment("Скорость по оси Х");
        $vx->setValue(28.7);
        $parameterCategory->addParameter($vx);

        $vy = new Parameter();
        WebApp::$EntityManager->persist($vy);
        $vy->setName("temp2");
        $vy->setTitle("Vy");
        $vy->setType("Float");
        $vy->setUnits("м/с");
        $vy->setComment("Скорость по оси Y");
        $vy->setValue(0);
        $parameterCategory->addParameter($vy);

        $vz = new Parameter();
        WebApp::$EntityManager->persist($vz);
        $vz->setName("temp3");
        $vz->setTitle("Vz");
        $vz->setType("Float");
        $vz->setUnits("м/с");
        $vz->setComment("Скорость по оси Z");
        $vz->setValue(0);
        $parameterCategory->addParameter($vz);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();

        echo "new Testbed's id: ", $testbed->getId(), "<br><br>\n";
        $testbeds = WebApp::$EntityManager->getRepository('Testbed')->getRecentTestbeds(8);
        foreach($testbeds as $testbed)
        {
            echo $testbed->getId(), " - ", $testbed->getName(), " (", $testbed->getVersion(), ")<br>\n";
            foreach($testbed->getCategories() as $category)
            {
                echo " - ", $category->getName(), "<br>\n";
                foreach($category->getParameters() as $parameter)
                {
                    echo " - - ", $parameter->getName(), " / ", $parameter->getType(), "<br>\n";
                }
            }
        }
    }

    public function installFCP(Array $request)
    {
//        CAEServerClient::IndexAllProjects();

        self::ChemProc(); // Koledina.
        self::ac2d(); // Kulikov.
        self::chemtrans(); // Chernykh.
        self::thinfilm(); // Kirienko.

        self::apim(); // Sozykin.
        self::scimq(); // Schapov.
        self::htm(); // Mishchenko.
        self::scale(); // Poluyanov.
        self::mdsim(); // Marin.
    }

    private function ChemProc()
    {
        self::removeExistingTestbedWithGuid("ChemProc");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("ChemProc");
        $testbed->setName("ChemProc");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("parameters");
        $parameterCategory->setTitle("File name");

        $vx = new Parameter();
        WebApp::$EntityManager->persist($vx);
        $vx->setName("inputFile");
        $vx->setTitle("File name");
        $vx->setType("File");
        $vx->setUnits("");
        $vx->setComment("");
        $vx->setValue("GA_OB_DH.DAT");
        $parameterCategory->addParameter($vx);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function ac2d()
    {
        self::removeExistingTestbedWithGuid("ac2d");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("ac2d");
        $testbed->setName("ac2d");
        $testbed->setState("AVAILABLE");

        WebApp::$EntityManager->flush();
    }

    private function chemtrans()
    {
        self::removeExistingTestbedWithGuid("chemtrans");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("chemtrans");
        $testbed->setName("chemtrans");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("first_pair");
        $parameterCategory->setTitle("Файл входных параметров");

        $parameter = new Parameter();
        WebApp::$EntityManager->persist($parameter);
        $parameter->setName("first_parameter");
        $parameter->setTitle("Температура в Кельвинах");
        $parameter->setType("File");
        $parameter->setUnits("");
        $parameter->setComment("Имя файла");
        $parameter->setValue("");
        $parameterCategory->addParameter($parameter);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function thinfilm()
    {
        self::removeExistingTestbedWithGuid("thinfilm");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("thinfilm");
        $testbed->setName("Magnetization of ultrathin film");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("parameters");
        $parameterCategory->setTitle("Параметры задачи");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("monolayers");
        $parameter1->setTitle("Целое число, большее единицы");
        $parameter1->setType("Integer");
        $parameter1->setUnits("число");
        $parameter1->setComment("Число монослоёв плёнки");
        $parameter1->setValue(2);
        $parameterCategory->addParameter($parameter1);

        $parameter2 = new Parameter();
        WebApp::$EntityManager->persist($parameter2);
        $parameter2->setName("eps");
        $parameter2->setTitle("Дробное положительное число");
        $parameter2->setType("Float");
        $parameter2->setUnits("число");
        $parameter2->setComment("Точность вычисления температуры Кюри");
        $parameter2->setValue(0.1);
        $parameterCategory->addParameter($parameter2);

        $parameter3 = new Parameter();
        WebApp::$EntityManager->persist($parameter3);
        $parameter3->setName("solver");
        $parameter3->setTitle("Строка, задающая тип решателя");
        $parameter3->setType("String");
        $parameter3->setUnits("");
        $parameter3->setComment("Решатель системы трансцендентных уравнений для определения намагниченности");
        $parameter3->setValue("scipy_fsolve");
        $parameterCategory->addParameter($parameter3);

        $parameter4 = new Parameter();
        WebApp::$EntityManager->persist($parameter4);
        $parameter4->setName("picFormat");
        $parameter4->setTitle("Строка формата выходного рисунка");
        $parameter4->setType("String");
        $parameter4->setUnits("");
        $parameter4->setComment("Формат выходного рисунка кривой намагниченности");
        $parameter4->setValue("PNG");
        $parameterCategory->addParameter($parameter4);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function apim()
    {
        self::removeExistingTestbedWithGuid("apim");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("apim");
        $testbed->setName("apim");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("processor_class_name");
        $parameterCategory->setTitle("Выполняемая операция");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("processor_class_name");
        $parameter1->setTitle("Имя класса, реализующего операцию");
        $parameter1->setType("String");
        $parameter1->setComment("Имя Java класса, который реализует нужную операцию обработки изображений");
        $parameter1->setValue("ru.uran.imm.hadoopimage.client.Nope");
        $parameterCategory->addParameter($parameter1);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.EdgeDetection");
        $parameter1->addParameterEnum($parameterEnum);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.Resize");
        $parameter1->addParameterEnum($parameterEnum);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.Convert");
        $parameter1->addParameterEnum($parameterEnum);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.FFT");
        $parameter1->addParameterEnum($parameterEnum);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.MatrixFilter");
        $parameter1->addParameterEnum($parameterEnum);

        $parameterEnum = new ParameterEnum();
        WebApp::$EntityManager->persist($parameterEnum);
        $parameterEnum->setValue("ru.uran.imm.hadoopimage.client.Nope");
        $parameter1->addParameterEnum($parameterEnum);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function scimq()
    {
        self::removeExistingTestbedWithGuid("scimq");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("scimq");
        $testbed->setName("scimq");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("app_cmdline");
        $parameterCategory->setTitle("Параметры запуска");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("ARGUMENTS");
        $parameter1->setTitle("Аргументы командной строки");
        $parameter1->setType("String");
        $parameter1->setUnits("");
        $parameter1->setComment("Справка: scimq -h");
        $parameter1->setValue("-h");
        $parameterCategory->addParameter($parameter1);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function htm()
    {
        self::removeExistingTestbedWithGuid("htm");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("htm");
        $testbed->setName("HultTuringMachine");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("first_pair");
        $parameterCategory->setTitle("Первая пара параметров");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("min");
        $parameter1->setTitle("Целое число");
        $parameter1->setType("Integer");
        $parameter1->setUnits("");
        $parameter1->setComment("Начальное значение размера машины Тьюринга");
        $parameter1->setValue(80000);
        $parameterCategory->addParameter($parameter1);

        $parameter2 = new Parameter();
        WebApp::$EntityManager->persist($parameter2);
        $parameter2->setName("max");
        $parameter2->setTitle("Целое число");
        $parameter2->setType("Integer");
        $parameter2->setUnits("");
        $parameter2->setComment("Конечное значение размера машины Тьюринга");
        $parameter2->setValue(100000);
        $parameterCategory->addParameter($parameter2);

        $parameter3 = new Parameter();
        WebApp::$EntityManager->persist($parameter3);
        $parameter3->setName("step");
        $parameter3->setTitle("Целое число");
        $parameter3->setType("Integer");
        $parameter3->setUnits("");
        $parameter3->setComment("Шаг изменения размера машины Тьюринга");
        $parameter3->setValue(10000);
        $parameterCategory->addParameter($parameter3);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function scale()
    {
        self::removeExistingTestbedWithGuid("scale");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("scale");
        $testbed->setName("Построение диагностических шкал");
        $testbed->setState("AVAILABLE");

        $parameterCategory = new Category();
        WebApp::$EntityManager->persist($parameterCategory);
        $parameterCategory->setName("start_param");
        $parameterCategory->setTitle("Параметры запуска стенда");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("file_name");
        $parameter1->setTitle("Файл с параметрами запуска стенда");
        $parameter1->setType("File");
        $parameter1->setUnits("");
        $parameter1->setComment("Задает путь к входному файлу");
        $parameter1->setValue("input.txt");
        $parameterCategory->addParameter($parameter1);

        $testbed->AddCategory($parameterCategory);

        WebApp::$EntityManager->flush();
    }

    private function mdsim()
    {
        self::removeExistingTestbedWithGuid("mdsim");

        $testbed = new Testbed();
        WebApp::$EntityManager->persist($testbed);
        $testbed->setGuid("mdsim");
        $testbed->setName("mdsim");
        $testbed->setState("AVAILABLE");

        $physicsCategory = new Category();
        WebApp::$EntityManager->persist($physicsCategory);
        $physicsCategory->setName("physics");
        $physicsCategory->setTitle("Physics characteristics");

        $parameter1 = new Parameter();
        WebApp::$EntityManager->persist($parameter1);
        $parameter1->setName("nperdim");
        $parameter1->setTitle("Integer number");
        $parameter1->setType("Integer");
        $parameter1->setUnits("");
        $parameter1->setComment("Number of particles per dimension");
        $parameter1->setValue(10);
        $physicsCategory->addParameter($parameter1);

        $parameter2 = new Parameter();
        WebApp::$EntityManager->persist($parameter2);
        $parameter2->setName("density");
        $parameter2->setTitle("Float point number");
        $parameter2->setType("Float");
        $parameter2->setUnits("");
        $parameter2->setComment("Dimensionless density");
        $parameter2->setValue(0.6);
        $physicsCategory->addParameter($parameter2);

        $parameter3 = new Parameter();
        WebApp::$EntityManager->persist($parameter3);
        $parameter3->setName("temperature");
        $parameter3->setTitle("Float point number");
        $parameter3->setType("Float");
        $parameter3->setUnits("");
        $parameter3->setComment("Dimensionless temperature");
        $parameter3->setValue(2.0);
        $physicsCategory->addParameter($parameter3);

        $parameter4 = new Parameter();
        WebApp::$EntityManager->persist($parameter4);
        $parameter4->setName("rcutoff");
        $parameter4->setTitle("Float point number");
        $parameter4->setType("Float");
        $parameter4->setUnits("");
        $parameter4->setComment("Dimensionless cut off radius");
        $parameter4->setValue(2.5);
        $physicsCategory->addParameter($parameter4);

        $parameter5 = new Parameter();
        WebApp::$EntityManager->persist($parameter5);
        $parameter5->setName("deltat");
        $parameter5->setTitle("Float point number");
        $parameter5->setType("Float");
        $parameter5->setUnits("");
        $parameter5->setComment("Dimensionless delta time step");
        $parameter5->setValue(0.0005);
        $physicsCategory->addParameter($parameter5);

        $parameter6 = new Parameter();
        WebApp::$EntityManager->persist($parameter6);
        $parameter6->setName("ntimesteps");
        $parameter6->setTitle("Integer number");
        $parameter6->setType("Integer");
        $parameter6->setUnits("");
        $parameter6->setComment("Number of time steps");
        $parameter6->setValue(1);
        $physicsCategory->addParameter($parameter6);

        $testbed->AddCategory($physicsCategory);

        $calcCategory = new Category();
        WebApp::$EntityManager->persist($calcCategory);
        $calcCategory->setName("calc");
        $calcCategory->setTitle("Computation parameters");

        $parameter7 = new Parameter();
        WebApp::$EntityManager->persist($parameter7);
        $parameter7->setName("levelmax");
        $parameter7->setTitle("Integer number");
        $parameter7->setType("Integer");
        $parameter7->setUnits("");
        $parameter7->setComment("Maximal leaf level of data structure");
        $parameter7->setValue(8);
        $calcCategory->addParameter($parameter7);

        $parameter8 = new Parameter();
        WebApp::$EntityManager->persist($parameter8);
        $parameter8->setName("sopt");
        $parameter8->setTitle("Integer number");
        $parameter8->setType("Integer");
        $parameter8->setUnits("");
        $parameter8->setComment("Number of particles per box at leaf level of data structure");
        $parameter8->setValue(10);
        $calcCategory->addParameter($parameter8);

        $testbed->AddCategory($calcCategory);

        WebApp::$EntityManager->flush();
    }

    private function removeExistingTestbedWithGuid($guid)
    {
        $testbed = WebApp::$EntityManager->getRepository('Testbed')->findOneBy(array('guid' => $guid));

        if ($testbed !== null)
        {
            foreach($testbed->getCategories() as $category)
            {
                foreach($category->getParameters() as $parameter)
                {
                    WebApp::$EntityManager->remove($parameter);
                }
                WebApp::$EntityManager->remove($category);
            }
            WebApp::$EntityManager->remove($testbed);
            WebApp::$EntityManager->flush();
        }
    }
}
?>
