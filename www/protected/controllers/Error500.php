<?php
require_once WEB_APP_PATH."base/Controller.php";
//require_once WEB_APP_PATH."view/Error404Page.php";

class Error500 extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/(.+)\/?/' => "showError",
            ));
    }

    public function defaultAction()
    {
        header("HTTP/1.0 500 Internal Server Error");

        $view = new Error404Page();
        $view->printPageSource();

        exit;
    }

    public function showError($request)
    {
        header("HTTP/1.0 500 Internal Server Error");
        die("500 Internal Server Error<br>$request[0]");
    }
}
?>
