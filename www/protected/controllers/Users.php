<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."models/UserPreferencesForm.php";

class Users extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
                '/^([0-9]+)\/?$/i' => "showInfo",
                '/^([0-9]+)\/Preferences\/?$/i' => "preferences",
            ));
    }

    protected function doActionProtection()
    {
        if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("account/signin/", true);
        }
    }

    public function defaultAction()
    {
        die("Users page");
        $this->view = new View("testbeds/list");

        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Испытательные стенды";

        $currentPage = 1;
        $testbedsPerPage = 2;
        $this->view['testbeds'] = WebApp::$EntityManager->getRepository("Testbed")->getTestbedsOnPage($currentPage, $testbedsPerPage);

        $this->view->printPageSource();
    }

    public function showInfo(Array $request)
    {
        $userId = (int)$request[0];
        if (WebApp::$User->getEntity()->getId() === $userId)
        {
            die("Information about user with id '".$userId."', <a href='/Users/".$userId."/Preferences/'>Preferences</a>");
        }
        else
        {
            die("Information about user with id '".$userId."'");
        }
    }

    public function preferences(Array $request)
    {
        if ((int)$request[0] !== (int)WebApp::$User->getEntity()->getId())
            WebAppUser::Redirect("Users/".$request[0]."/");

        $this->view = new View("users/preferences");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Настройки пользователя";

        $this->view['preferencesForm'] = new UserPreferencesForm();
        if (isset($_POST['SubmitButton']))
        {
            if (md5(md5($_POST['user_password'])) !== WebApp::$User->getEntity()->getPassword())
                $this->view['message'] = "Wrong current password";
            else
            {
                WebApp::$User->getEntity()->setName($_POST['user_name']);
                WebApp::$User->getEntity()->setEmail($_POST['user_email']);
                if (isset($_POST['new_user_password']) && isset($_POST['repeat_new_user_password'])
                       && $_POST['new_user_password'] === $_POST['repeat_new_user_password']
                       && $_POST['repeat_new_user_password'] !== "")
                {
                    WebApp::$User->getEntity()->setPassword(md5(md5($_POST['new_user_password'])));
                    if (isset($_COOKIE['password']))
                        setcookie('password', WebApp::$User->getEntity()->getPassword(), time() + 3600 * 24, '/');
                }
                WebApp::$EntityManager->flush();
            }
        }
        $this->view['preferencesForm']->userName = WebApp::$User->getEntity()->getName();
        $this->view['preferencesForm']->email = WebApp::$User->getEntity()->getEmail();

        $this->view->printPageSource();
    }
}
?>
