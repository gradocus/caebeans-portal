<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."helpers/recursive_remove_directory.php";
require_once WEB_APP_PATH."helpers/zip_files_in_folder.php";

class Upload extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
//                '/^([0-9]+)\/?$/i' => "uploadFile",
            ));
    }

    protected function doActionProtection()
    {
        if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("account/signin/", true);
        }
    }

    public function defaultAction()
    {
        if (isset($_POST['testbed']));
            $this->uploadFile(array($_POST['testbed']));
    }

    public function uploadFile(Array $request)
    {
        ini_set("default_socket_timeout", 3600);
        ini_set("max_execution_time", 3600);

        $inputFile = NULL;
        $inputFileName = "";
        if (count($_FILES) === 1)
        {
            foreach ($_FILES as $fileName => $file)
            {
                $inputFile = $file;
                $inputFileName = $fileName;
            }
        }

        $this->view = new View("json/view", "json");

        if ($inputFile === NULL)
        {
            $this->view['status'] = "Error";
            $this->view->printPageSource();
            return;
        }

        $testbedId = (int)$request[0];
        $destDirectory = WEB_APP_PATH."uploadedFiles/".WebApp::$User->getEntity()->getId()."_".$testbedId;

        if (!file_exists($destDirectory) || !is_dir($destDirectory))
            mkdir($destDirectory);

        $extension = pathinfo($inputFile['name'], PATHINFO_EXTENSION);
        $destFileName = $inputFileName.($extension != "" ? ".".$extension : "");
        if (move_uploaded_file($inputFile['tmp_name'], $destDirectory."/".$destFileName))
        {
            $this->view['status'] = "Uploaded";
            $this->view['fileName'] = $destFileName;

            $uploadedFile = new UploadedFile();

            $uploadedFile->setUser(WebApp::$User->getEntity());
            $uploadedFile->setTestbed(WebApp::$EntityManager->find('Testbed', $testbedId));
            $uploadedFile->setUserFileName($inputFile['name']);
            $uploadedFile->setFileName($destFileName);

            WebApp::$EntityManager->persist($uploadedFile);
            WebApp::$EntityManager->flush();
        }
        else
        {
            $this->view['status'] = "Error";
        }

        $this->view->printPageSource();
    }
}
?>
