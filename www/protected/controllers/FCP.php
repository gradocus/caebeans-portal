<?php

require_once WEB_APP_PATH."base/Controller.php";
require_once WEB_APP_PATH."views/JobsPage.php";

require_once WEB_APP_PATH."helpers/CAEInstanceClient.php";

class FCP extends Controller
{
    public function __construct()
    {
        parent::__construct(array(
/*                '/^page_([0-9]+)\/?$/i' => "showJobsPage",

                '/^([0-9]+)\/?$/i' => "showJobData",
                '/^([0-9]+)\/results\/?$/i' => "getJobResults",
                '/^([0-9]+)\/works\/?$/i' => "getJobWorks",
                '/^([0-9]+)\/remove\/?$/i' => "removeJob",
*/            ));
    }

    protected function doActionProtection()
    {
        /*if (!WebApp::$User->isAuthorised())
        {
            WebApp::$User->redirect("Auth/Login", true);
        }*/
    }

    public function defaultAction()
    {
        $this->showJobsPage(array(1));
    }

    public function showJobsPage(Array $request)
    {
        $this->view = new View("fcp/view", "main");
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Материалы для 
участников гранта ФЦП";
        $this->view->printPageSource();
    }

    public function showJobData(Array $request)
    {
        $this->view = new View("jobs/view");

        $this->view['jobId'] = (int)$request[0];
        $this->view['job'] = WebApp::$EntityManager->find("Job", $this->view['jobId']);
        $this->view['parameterValues'] = $this->view['job']->getParameterValues();
        $this->view['testbed'] = $this->view['job']->getTestbed();
        $this->view['pageTitle'] = WebApp::$Properties['application']['title']." / Задачи / ".$this->view['job']->getName();

        $this->view->printPageSource();
    }

    public function getJobResults(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);

        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=".$job->getGuid().".zip");

        echo CAEInstanceClient::GetResultsArch($job->getGuid());
        exit;
    }

    public function getJobWorks(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);

        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=".$job->getGuid().".zip");

        echo CAEInstanceClient::GetWorkArch($job->getGuid());
        exit;
    }

    public function removeJob(Array $request)
    {
        $job = WebApp::$EntityManager->find("Job", (int)$request[0]);
        if ($job->getState() !== 'RUNNING')
        {
            foreach ($job->getParameterValues() as $parameterValue)
                WebApp::$EntityManager->remove($parameterValue);
            if ($job->getState() === 'SUCCESSFULL')
                CAEInstanceClient::RemoveResultDir($job->getGuid());
            WebApp::$EntityManager->remove($job);
            WebApp::$EntityManager->flush();

            WebApp::$User->redirect("Jobs");
        } else {
            WebApp::$User->redirect("Jobs?message=error");
        }
    }
}
?>
