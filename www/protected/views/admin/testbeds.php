    <style type="text/css">
      .table th, .table td {
        text-align: center;
        vertical-align: middle;
      }
      .table th:first-child, .table td:first-child {
        width: 42%;
        padding-left: 15px;
        text-align: left;
      }
      tr.unavailable {
        color: #999999;
      }
      .rowlink > td {
        cursor: pointer;
      }
      .rowlink > td.nolink {
        cursor: auto;
      }
    </style>
        <h4>
          <ul class="nav nav-tabs">
            <li class="nav-tabs-header">Testbeds:</li>
            <li class="active"><a href="#portal-testbeds" data-toggle="tab">Portal Testbeds</a></li>
            <li><a href="#server-but-portal-testbeds" data-toggle="tab">Server Testbeds</a></li>
          </ul>
        </h4>

        <div class="tab-content">
          <div class="tab-pane" id="server-but-portal-testbeds">
            <table class="table table-condensed table-striped table-hover">
              <thead>
                <tr>
                  <th>Testbed name</th>
                  <th>Author</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="rowlink" data-provides="rowlink">
                <tr><td colspan="4" style="text-align: center;"><img src="/img/load.gif" alt="Loading..." title="Loading..."></td></tr>
<?php foreach ($viewData['testbeds'] as $testbed): ?>
                <tr>
                  <td><a href="/admin/testbeds/server/<?php echo $testbed->getGuid(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</a></td>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td class="nolink">
                    <a class="btn btn-warning btn-mini" title="Request for testbed" href="/admin/testbeds/server/<?php echo $testbed->getGuid(); ?>/request/"><i class="icon-share icon-white"></i></a>
                  </td>
                </tr>
<?php endforeach; ?>
              </tbody>
            </table>
          </div>

          <div class="tab-pane active" id="portal-testbeds">
            <table class="table table-condensed table-striped table-hover">
              <thead>
                <tr>
                  <th>Testbed name</th>
                  <th>Author</th>
                  <th>Jobs</th>
                  <th>State</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="rowlink" data-provides="rowlink">
<?php foreach ($viewData['portalTestbeds'] as $testbed): ?>
                <tr>
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</a></td>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td><?php echo count($testbed->getJobs()); ?></td>
                  <td class="nolink"><select class="btn btn-mini span2" href="/admin/testbeds/<?php echo $testbed->getId(); ?>/" onChange="changeTestbedState(this);">
                    <option<?php if ($testbed->getState() == "HIDDEN") echo " selected"; ?>>HIDDEN</option>
                    <option<?php if ($testbed->getState() == "SUSPENDED") echo " selected"; ?>>SUSPENDED</option>
                    <option<?php if ($testbed->getState() == "AVAILABLE") echo " selected"; ?>>AVAILABLE</option>
                  </select></td>
                  <td class="nolink">
<!--                    <a class="btn btn-danger btn-mini" title="Stop job submitting" href="#"><i class="icon-stop icon-white"></i></a>-->
                    <a class="btn btn-danger btn-mini" title="Delete from portal" href="/admin/testbeds/<?php echo $testbed->getId(); ?>/remove/" onClick="removeFromPortal(this); return false;"><i class="icon-ban-circle icon-white"></i></a>
                  </td>
                </tr>
<?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
<!--
       <div class="row-fluid">
         <div class="span5">
           DiVTB Server Testbeds:<br>
           <select id="server-testbeds" class="span15" style="height: 200px;" multiple>
<?php foreach ($viewData['testbeds'] as $testbed): ?>
             <option value="<?php echo $testbed->getGuid(); ?>"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</option>
<?php endforeach; ?>
           </select>
         </div>
         <div class="span2" style="vertical-align: middle;">
           <p><a class="btn btn-primary btn-mini" onClick="addSelectedTestbeds();"><i class="icon-forward icon-white"></i></a></p>
           <p><a class="btn btn-primary btn-mini" onClick="removeSelectedTestbeds();"><i class="icon-backward icon-white"></i></a></p>
         </div>
         <div class="span5">
           DiVTB Portal Testbeds:<br>
           <select id="portal-testbeds" class="span15" style="height: 200px;" multiple>
<?php foreach ($viewData['portalTestbeds'] as $testbed): ?>
             <option value="<?php echo $testbed->getId(); ?>"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</option>
<?php endforeach; ?>
           </select>
         </div>
       </div> 
-->
<script>

function changeTestbedState(testbedState)
{
    var newTestbedState = $(testbedState).val();
    var testbedUrl = $(testbedState).attr('href');
    if (newTestbedState == 'HIDDEN')
    {
        testbedUrl += "hide/";
    }
    else if (newTestbedState == 'AVAILABLE')
    {
        testbedUrl += "activate/";
    }
    else if (newTestbedState == 'SUSPENDED')
    {
        testbedUrl += "suspend/";
    }

    $.ajax({type: "GET",
            url: testbedUrl
        }).done(function(data) {
            var response = $.parseJSON(data);
            if (response.result == "error")
            {
                $('#MessageBoxTitle').text("Error");
                $('#MessageBoxBody').text(response.error);
                $('#MessageBox').modal();
            }
        }).fail(function(xhr) {
            $('#MessageBoxTitle').text("Error");
            $('#MessageBoxBody').text("Что-то пошло не так. Повторите Ваш запрос еще раз.");
            $('#MessageBox').modal();
        });
}

function removeFromPortal(removeButton)
{
    $(removeButton).attr("onClick", "").addClass("disabled").attr("title", "Removing...");
    var testbedRow = $(removeButton).parent().parent();
    var removeTestbedUrl = $(removeButton).attr('href');

    $.ajax({type: "GET",
            url: removeTestbedUrl
        }).done(function(data) {
            var response = $.parseJSON(data);
            if (response.result == "error")
            {
                $('#MessageBoxTitle').text("Error");
                $('#MessageBoxBody').text(response.error);
                $('#MessageBox').modal();
                $(removeButton).attr("onClick", "removeFromPortal(this); return false;").removeClass("disabled").attr("title", "Remove");
            }
            else
            {
                location.reload();
                //$(testbedRow).remove();
            }
        }).fail(function(xhr) {
            $('#MessageBoxTitle').text("Error");
            $('#MessageBoxBody').text("Что-то пошло не так. Повторите Ваш запрос еще раз.");
            $('#MessageBox').modal();
            $(removeButton).attr("onClick", "removeFromPortal(this); return false;").removeClass("disabled").attr("title", "Remove");
        });
}

function addTestbed(addTestbedButton)
{
    var testbedRow = $(addTestbedButton).parent().parent();
    var projectId = $(testbedRow).attr('projectId');
    var caebeanId = $(testbedRow).attr('caebeanId');

    $.post("/admin/testbeds/server/add/", { projectId: projectId, caebeanId: caebeanId }).done(function(data) {
        alert("Added");
        location.reload();
        //var response = $.parseJSON(data);
    }).fail(function(xhr) {
            $('#MessageBoxTitle').text("Error");
            $('#MessageBoxBody').text("Что-то пошло не так. Повторите Ваш запрос еще раз.");
            $('#MessageBox').modal();
        });

    return false;
}

$(document).ready(function() {
    var serverTestbedsTableBody = $('#server-but-portal-testbeds').find('table tbody');
    $.ajax({type: "GET",
        url: "/admin/testbeds/server/",
        dataType: 'json'
    }).done(function(response) {
        response.testbeds.forEach(function(testbed) {
            var newTestbedRow = "<tr projectId=\"" + testbed.project + "\" caebeanId=\"" + testbed.guid + "\">"
                                + "<td>" + testbed.name + " (" + testbed.version + ")</td>"
                                + "<td>" + testbed.author + "</td>"
                                + "<td class=\"nolink\"><a class=\"btn btn-warning btn-mini\" title=\"Add testbed to the portal\" href=\"/admin/testbeds/\" onClick=\"return addTestbed(this);\"><i class=\"icon-share icon-white\"></i></a></td>"
                              + "</tr>";
            $(serverTestbedsTableBody).find('tr:last').after(newTestbedRow);
        });
        var loadingRow = serverTestbedsTableBody.children()[0];
        $(loadingRow).remove();
    });
});

function addSelectedTestbeds()
{
    $('#server-testbeds option:selected').each(function()
    {
        var selectedOption = $(this);
        var testbedId = $(this).val();
        
    });
}

function removeSelectedTestbeds()
{
    $('#portal-testbeds option:selected').each(function()
    {
        var selectedOption = $(this);
        var testbedId = $(this).val();
        $.post("/admin/testbeds/" + testbedId + "/remove/?time=" + (new Date()).getTime(),
          function(response)
          {
              var result = jQuery.parseJSON(response);
              if (result.status == "removed")
              {
                  $(selectedOption).remove();
              }
              else
              {
                  $('#MessageBoxTitle').text("Error");
                  $('#MessageBoxBody').text(result.error);
                  $('#MessageBox').modal();
              }
          });
    });
}
</script>
