        <h4>
          <ul class="nav nav-tabs">
            <li class="nav-tabs-header">Account settings</li>
          </ul>
        </h4>
<?php
if (isset($viewData['message']))
{
?>
        <div class="alert alert-error">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <strong>Error!</strong> <?php echo $viewData['message']; ?>
        </div>
<?php
}
?>
        <form class="form-horizontal pull-center" action="" method="POST">
          <div class="control-group">
            <label class="control-label" for="input_lastName" rel="tooltip" title="Ваша фамилия, например, Иванов.">Фамилия</label>
            <div class="controls">
	    <input type="text" id="input_lastName" class="span3" name="lastName" placeholder="Фамилия" value="<?php echo $viewData['lastName'];?>">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_firstName" rel="tooltip" title="Ваше имя, например, Иван.">Имя</label>
            <div class="controls">
              <input type="text" id="input_firstName" class="span3" name="firstName" placeholder="Имя" value="<?php echo $viewData['firstName'];?>">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_middleName" rel="tooltip" title="Ваше отчество, например, Иванович.">Отчество</label>
            <div class="controls">
              <input type="text" id="input_middleName" class="span3" name="middleName" placeholder="Отчество" value="<?php echo $viewData['middleName'];?>">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_email" rel="tooltip" title="Адрес вашей электронной почты.">Электронная почта</label>
            <div class="controls">
              <input type="text" id="input_email" class="span3" name="email" placeholder="your.email@domain.com" value="<?php echo $viewData['email']; ?>">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_group" rel="tooltip" title="Группа.">Роль</label>
            <div class="controls">
              <select id="input_group" class="span3" name="group">
<?php if ($viewData['group'] == 'Administrator') { ?>
                <option selected>Administrator</option>
                <option>Moderator</option>
                <option>User</option>
<?php } elseif ($viewData['group'] == 'Moderator') { ?>
                <option>Administrator</option>
                <option selected>Moderator</option>
                <option>User</option>
<?php } elseif ($viewData['group'] == 'User') { ?>
                <option>Administrator</option>
                <option>Moderator</option>
                <option selected>User</option>
<?php } ?>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_company" rel="tooltip" title="Организация">Организация</label>
            <div class="controls">
              <input type="text" id="input_company" class="span3" name="company" placeholder="Организация" value="<?php echo $viewData['company']; ?>">
            </div>
          </div>

          <hr>

          <div class="control-group">
            <label class="control-label" for="input_newPassword1" rel="tooltip" title="Новый пароль.">Новый пароль</label>
            <div class="controls">
              <input type="password" id="input_newPassword1" class="span3" name="newPassword1" placeholder="Новый пароль" value="">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="input_newPassword2" rel="tooltip" title="Повторите новый пароль.">Повторите пароль</label>
            <div class="controls">
              <input type="password" id="input_newPassword2" class="span3" name="newPassword2" placeholder="Новый пароль" value="">
            </div>
          </div>
          <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn btn-success" name="submit" value="save">Save</button>
                <a class="btn" href="/admin/users/">Cancel</a>
              </div>
          </div>
        </form>
      </div>
