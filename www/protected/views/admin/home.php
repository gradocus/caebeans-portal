    <style type="text/css">
      .table th, .table td {
        text-align: center;
        vertical-align: middle;
      }
      .table th:first-child, .table td:first-child {
        width: 0;
        margin: 0;
        padding: 0;
        border: none;
      }
      .rowlink > td {
        cursor: pointer;
      }
      thead > tr > th.job-title,
      .rowlink > td.job-title {
        width: 60%;
        text-align: left;
      }
      .rowlink > td.nolink {
        cursor: auto;
      }
      .info > td.no-items {
        padding: 5px 0 5px 0;
      }
    </style>
        <h4>
          <ul class="nav nav-tabs">
            <li class="nav-tabs-header">Requests for Testbeds</li>
          </ul>
        </h4>
<table id="jobs-table" class="table table-condensed table-hover">
  <thead>
    <tr>
      <th></th>
      <th class="job-title">User</th>
      <th>Testbed</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody class="rowlink" data-provides="rowlink">
<?php if (count($viewData['requests']) == 0): ?>
    <tr class="info">
      <td class="no-items" colspan="4">No requests found.</td>
    </tr>
<?php else: foreach ($viewData['requests'] as $request): ?>
    <tr>
      <td><a href="#"></a></td>
      <td class="job-title">
        <a href="/admin/users/<?php echo $request->getUser()->getId(); ?>/">
          <?php echo $request->getUser()->getFirstName(), " ", $request->getUser()->getMiddleName(), " ", $request->getUser()->getLastName(); ?>
        </a>
      </td>
      <td>
        <a href="/testbeds/<?php echo $request->getTestbed()->getId(); ?>/">
          <?php echo $request->getTestbed()->getName(), ' (', $request->getTestbed()->getVersion(), ')'; ?>
        </a>
      </td>
      <td class="nolink">
        <a class="btn btn-success btn-mini" title="Allow" href="/admin/users/<?php echo $request->getUser()->getId(); ?>/allowRequestForTestbed/<?php echo $request->getTestbed()->getId(); ?>/"><i class="icon-ok icon-white"></i></a>
        <a class="btn btn-danger btn-mini" title="Disallow" href="/admin/users/<?php echo $request->getUser()->getId(); ?>/disallowRequestForTestbed/<?php echo $request->getTestbed()->getId(); ?>/"><i class="icon-ban-circle icon-white"></i></a>
      </td>
    </tr>
<?php endforeach; endif; ?>
  </tbody>
</table>

<h4>
  <ul class="nav nav-tabs">
    <li class="nav-tabs-header">Inactive users</li>
  </ul>
</h4>
<table id="jobs-table" class="table table-condensed table-hover">
  <thead>
    <tr>
      <th></th>
      <th class="job-title">Full Name</th>
      <th>Email</th>
      <th>Group</th>
      <th>Active</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody class="rowlink" data-provides="rowlink">
<?php if (count($viewData['users']) == 0): ?>
    <tr class="info">
      <td class="no-items" colspan="6">No inactive users found.</td>
    </tr>
<?php else: foreach ($viewData['users'] as $user): ?>
    <tr class="<?php if ($user->isBanned())
                     echo "error";
                 elseif ($user->getName() == "Administrator")
                     echo "success";
                 elseif ($user->getName() == "Moderator")
                     echo "warning";
                 else
                     echo "info"; ?>">
      <td><a href="/admin/users/<?php echo $user->getId(); ?>/"></a></td>
      <td class="job-title"><?php echo $user->getFirstName(), " ", $user->getMiddleName(), " ", $user->getLastName(); ?></td>
      <td><?php echo $user->getEmail(); ?></td>
      <td><?php echo $user->getName(); ?></td>
      <td class="nolink">
        <input type="checkbox" onClick="return onCheckboxClick(this);" value="<?php echo (!$user->isBanned() ? "on" : "off"); ?>" <?php echo (!$user->isBanned() ? "checked " : ""); ?>/>
      </td>
      <td class="nolink">
<!--        <a class="btn btn-mini disabled"><i class="icon-download-alt"></i></a>-->
        <a class="btn btn-danger btn-mini" title="Remove user" onClick="return confirm('Are you sure? This will remove user forever.');" href="/admin/users/<?php echo $user->getId(); ?>/remove/"><i class="icon-remove icon-white"></i></a>
      </td>
    </tr>
<?php endforeach; endif; ?>
  </tbody>
</table>

<script>
function onCheckboxClick(checkbox)
{
    var action = $(checkbox).val() == 'off' ? "activate" : "deactivate";
    var userRow = $(checkbox).parent().parent();
    var userURL = $(userRow).find('td a').attr("href").replace(/\/$/, "");

    $.post(userURL + "/" + action + "/" + "?time=" + (new Date()).getTime(),
          function(response)
          {
              var result = jQuery.parseJSON(response);
              if (result.status == "activated")
              {
                  $(checkbox).val('on');
              }
              else if (result.status == "deactivated")
              {
                  $(checkbox).val('off');
              }
              else
              {
                  $('#MessageBoxTitle').text("Error");
                  $('#MessageBoxBody').text(result.error);
                  $('#MessageBox').modal();
                  $(checkbox).prop('checked', true);

                  return false;
              }
          });
    return true;
}
</script>
