<?php

require_once WEB_APP_PATH."base/View.php";

class HomePage extends View
{
    public function __construct()
    {
        parent::__construct();
    }

    public function printPageSource()
    {
        $pageTitle = WebApp::$Properties['application']['title']." / Главная";

        $contentScript = "jobs/view.php";

        include WEB_APP_PATH."views/layouts/home.php";
    }
}
?>
