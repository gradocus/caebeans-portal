<?php

require_once WEB_APP_PATH."base/View.php";

class JobsPage extends View
{
    public function __construct()
    {
        parent::__construct();
    }

    public function printPageSource()
    {
        $pageTitle = WebApp::$User->GetEntity()->GetName()." / Менеджер задач &mdash; ".WebApp::$Properties['application']['title'];

        $contentScript = WEB_APP_PATH."views/jobs/view.php";

        include WEB_APP_PATH."views/layouts/main.php";
    }
}
?>
