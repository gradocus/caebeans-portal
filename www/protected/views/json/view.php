<?php

function isAssoc(Array $array)
{
    return count(array_filter(array_keys($array), 'is_string')) > 0;
}

function printJSON(Array $array, $indent = "\t")
{
    $isFirst = true;
    if (isAssoc($array))
    {
        foreach($array as $key => $value)
        {
            echo (!$isFirst ? ",\n" : ''), $indent, '"', $key, '": ';
            $isFirst = false;
            if (!is_array($value))
            {
                echo '"', $value, '"';
            }
            elseif (isAssoc($value))
            {
                echo "{\n";
                printJSON($value, $indent."\t");
                echo $indent, "}";
            }
            else
            {
                echo "[\n";
                printJSON($value, $indent."\t");
                echo $indent, "]";
            }
        }
    }
    else
    {
        foreach($array as $value)
        {
            echo (!$isFirst ? ",\n" : ''), $indent;
            $isFirst = false;
            if (is_array($value))
            {
                echo "{\n";
                printJSON($value, $indent."\t");
                echo $indent, '}';
            }
            else
            {
                echo '"', $value, '"';
            }
        }
    }
    echo "\n";
}

//printJSON($viewData);
echo json_encode($viewData);
?>
