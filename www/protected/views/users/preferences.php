<div class="cont">
				<div class="header">
					Настройки: <?php echo $viewData['preferencesForm']->userName; ?>
				</div>
				<div class="body"><form action="" method="POST">
    <div class="generator">
    <ul>
        <li>Основные настройки аккаунта:
        <ul>
            <li>Имя пользователя<br>
            <span class="description">Введите Ваше имя, например "Иванов Иван Иванович"</span>
            <div class="value">
                <input style="text-align: center; width: 200px;" type="text" name="user_name" value="<?php echo $viewData['preferencesForm']->userName; ?>">
            </div>
            <span class="description"></span>
            </li>
            <li>Адрес электронной почты<br>
            <span class="description">Адрес Вашей электронной почты, например "user.name@example.com"</span>
            <div class="value">
                <input style="text-align: center; width: 200px;" type="text" name="user_email" value="<?php echo $viewData['preferencesForm']->email; ?>">
            </div>
            <span class="description">Данный адрес будет использоваться в качестве логина при авторизации на портале.</span>
            </li>
            <li>Текущий пароль<br>
            <span class="description">Введите Ваш текущий пароль для сохранения изменений</span>
            <div class="value">
                <input style="text-align: center; width: 200px;" type="password" name="user_password" value="">
            </div>
            <span class="description"></span>
            </li>
			<li>Новый пароль<br>
            <span class="description">Введите новый пароль для авторизации</span>
            <div class="value">
                <input style="text-align: center; width: 200px;" type="password" name="new_user_password" value="">
            </div>
            <span class="description"></span>
            </li>
            <li>Повторите пароль<br>
            <span class="description">Повторите введенный ранее пароль</span>
            <div class="value">
                <input style="text-align: center; width: 200px;" type="password" name="repeat_new_user_password" value="">
            </div>
            <span class="description"></span>
            </li>
        </ul>
        </li>
        <?php if (isset($viewData['message'])) echo $viewData['message']; ?>
    </ul>
    </div>
    <div class="buttons">
    	<button title="Submit" name="SubmitButton" value="Submit">Submit</button>
    </div>
				</form></div>
			</div>
