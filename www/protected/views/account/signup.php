        <h4>Sign up</h4>

        <hr>

        <form class="form-horizontal pull-center" action="" method="post">
          <div class="control-group">
            <label class="control-label" for="inputLastName">Last name</label>
            <div class="controls">
              <input type="text" id="inputLastName" name="lastName" placeholder="Last name" value="<?php echo $viewData['lastName']; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputFirstName">First name</label>
            <div class="controls">
              <input type="text" id="inputFirstName" name="firstName" placeholder="First name" value="<?php echo $viewData['firstName']; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputMiddleName">Middle name</label>
            <div class="controls">
              <input type="text" id="inputMiddleName" name="middleName" placeholder="Middle name" value="<?php echo $viewData['middleName']; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputEmail">Email</label>
            <div class="controls">
              <input type="text" id="inputEmail" name="email" placeholder="Email" value="<?php echo $viewData['email']; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputCompany">Company</label>
            <div class="controls">
              <input type="text" id="inputCompany" name="company" placeholder="Company" value="<?php echo $viewData['company']; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputPassword1">Password</label>
            <div class="controls">
              <input type="password" id="inputPassword1" name="password1" placeholder="Password">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputPassword2">Password (again)</label>
            <div class="controls">
              <input type="password" id="inputPassword2" name="password2" placeholder="Password (again)">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div><!--
          <div class="control-group">
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox"> I have read <a href="#user-agreement">the agreement</a> and agree to all the terms
              </label>
            </div>
          </div>-->
<?php if (isset($viewData['message'])): ?>
          <div class="control-group">
            <div class="controls">
              <div class="alert alert-error">
                <?php echo $viewData['message']; ?>
              </div>
            </div>
          </div>
<?php endif; ?>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-primary">Sign up</button><a class="btn cancel" href="/">Cancel</a>
            </div>
          </div>
        </form>
    <script>
      function checkName(input) {
          var name = $(input).val().trim();
          var controlGroup = $(input).parent().parent();

          if (name == "") {
              $(controlGroup).attr('class', "control-group");
              $(input).val("");
              $(input).siblings('span').text("require");
              $(input).siblings('span').css('visibility', "hidden");
              return;
          }

          var error = /^[a-zа-я\-]+$/i.test(name) == false;
          if (error == true) {
              $(input).siblings('span').html('<a class="btn btn-mini btn-danger disabled"><i class="icon-remove icon-white"></i></a>');
              $(controlGroup).addClass("error");
              $(controlGroup).attr('class', "control-group error");
          } else {
              $(input).siblings('span').html('<a class="btn btn-mini btn-success disabled"><i class="icon-ok icon-white"></i></a>');
              $(controlGroup).addClass("success");
              $(controlGroup).attr('class', "control-group success");
          }

          $(input).siblings('span').css('visibility', "visible");
      }

      function validate_form(form) {
          window.location = "jobs.html";
          return false;
      }
    </script>
