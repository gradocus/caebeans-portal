<?php
require_once WEB_APP_PATH."base/View.php";

class LoginPage extends View
{
	public function __construct()
	{
		parent::__construct();
		
		$this->loginForm = null;
	}
	
	private $loginForm;
	
	public function setLoginForm($loginForm)
	{
		$this->loginForm = $loginForm;
	}
	
	public function printPageSource()
	{
		if ($this->loginForm === null)
			$this->loginForm = new LoginForm();
	
		if (isset(WebApp::$Properties['application']['title']))
			$title = WebApp::$Properties['application']['title'];
		else
			$title = WebApp::$Properties['name'];
		$pageSourceResult = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>{$title}</title>
</head>
<body>
	<form action="" method="post">
HTML;
		$rememberMeChecked = $this->loginForm->rememberMe ? " checked" : "";
		$pageSourceResult .= <<< HTML
		Логин: <input type="text" name="login" value="{$this->loginForm->login}"> {$this->loginForm->validationErrors['login']}<br>
		Пароль: <input type="password" name="password" value=""> {$this->loginForm->validationErrors['password']}<br>
		<label><input type="checkbox" name="rememberMe"{$rememberMeChecked}> Запомнить?</label><br>
		<button title="Логин" name="LoginForm" value="Submit">Логин</button>
	</form>
HTML;
		$i = 0;
		while(isset($this->loginForm->validationErrors[$i]))
		{
			$pageSourceResult .= <<< HTML
	{$this->loginForm->validationErrors[$i]}<br>
HTML;
			$i++;
		}
		$pageSourceResult .= <<< HTML
</body>
</html>
HTML;
		echo $pageSourceResult;
	}
}
?>
