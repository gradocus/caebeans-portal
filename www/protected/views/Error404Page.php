<?php
class Error404Page extends View
{
    public function __construct()
    {
        parent::__construct();
    }

    public function printPageSource()
    {
        $pageTitle = WebApp::$Properties['application']['title']." / 404 - Page not found";

        $contentScript = WEB_APP_PATH."views/errors/404.php";

        include WEB_APP_PATH."views/layouts/main.php";
    }
}
?>
