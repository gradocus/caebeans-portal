<div class="cont">
    <div class="header">Материалы для участников гранта ФЦП</div>
        <div class="body">
            Участникам доступны следующие материалы:
            <ul>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/Quick guide for the DiVTB 
system.pdf">Руководство по системе 
DiVTB</a></li>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/virtual_machine.pdf">Руководство к 
виртуальной машине 
DiVTB</a> на примере <a href="https://www.virtualbox.org/">Oracle VM VirtualBox</a></li>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/DiVTBMachine.zip">Виртуальная 
машина</a> (~792MB)</li>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/PowExample.zip">Пример интеграции 
проекта</a></li>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/DiVTBPortalTestbedViewer.zip">Визуализатор испытательных стендов</a></li>
                <li><a href="/36e30661ccf74ef0a72373ce3a592496/report_template.doc">Форма шаблона научного отчета</a></li>
            </ul>
        </div>
    </div>
</div>
