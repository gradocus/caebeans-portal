    <style type="text/css">
      .table th, .table td {
        text-align: center;
        vertical-align: middle;
      }
      .table th:first-child, .table td:first-child {
        width: 0;
        margin: 0;
        padding: 0;
        border: none;
      }
      .rowlink > td {
        cursor: pointer;
      }
      thead > tr > th.job-title,
      .rowlink > td.job-title {
        width: 60%;
        text-align: left;
      }
      .rowlink > td.nolink {
        cursor: auto;
      }
      .btn-round {
        border-radius: 125px;
        padding: 3px 4px 1px 6px;
      }
      .btn-round > i {
        margin: 1px 1px 0px 0px;
      }
    </style>
        <h4>
          <ul class="nav nav-tabs">
            <li class="nav-tabs-header"><?php echo $viewData['testbed']->getName(); ?>: Jobs</li>
          </ul>
        </h4>
<table id="jobs-table" class="table table-condensed table-hover">
  <thead>
    <tr>
      <th></th>
      <th><a href="#" title="Update all" onClick="refreshAll(this);"><i class="icon-refresh"></i></a></th>
      <th class="job-title">Job Name</th>
      <th>Start Time</th>
      <th>Finish Time</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody class="rowlink" data-provides="rowlink">
<?php
foreach (array_reverse($viewData['jobs']) as $job)
{
    switch ($job->getState())
    {
        case "SUCCESSFULL":
?>
    <tr class="success">
      <td><a href="/Jobs/<?php echo $job->getId(); ?>/"></a></td>
      <td><a class="btn btn-mini btn-round btn-success disabled" title="Success"><i class="icon-ok icon-white"></i></a></td>
      <td class="job-title"><?php echo $job->getName(); ?></td>
      <td><?php echo $job->getStartTime()->format('Y-m-d<\b\r>H:i:s'); ?></td>
      <td><?php echo ($job->getCompleteTime() <= $job->getStartTime() ? "????-??-??<br>??:??:??" : $job->getCompleteTime()->format('Y-m-d<\b\r>H:i:s')); ?></td>
      <td class="nolink">
        <a class="btn btn-success btn-mini" title="Download job results" href="/Jobs/<?php echo $job->getId(); ?>/Results/"><i class="icon-download-alt icon-white"></i></a>
        <a class="btn btn-danger btn-mini" title="Remove job" href="/Jobs/<?php echo $job->getId(); ?>/Remove/"><i class="icon-remove icon-white"></i></a>
      </td>
    </tr><?php
            break;
        case "FAILED":
        case "HELD":
?>
    <tr class="error">
      <td><a href="/Jobs/<?php echo $job->getId(); ?>/"></a></td>
      <td><a class="btn btn-mini btn-round btn-danger disabled" title="Error"><i class="icon-remove icon-white"></i></a></td>
      <td class="job-title"><?php echo $job->getName(); ?></td>
      <td><?php echo $job->getStartTime()->format('Y-m-d<\b\r>H:i:s'); ?></td>
      <td><?php echo ($job->getCompleteTime() <= $job->getStartTime() ? "????-??-??<br>??:??:??" : $job->getCompleteTime()->format('Y-m-d<\b\r>H:i:s')); ?></td>
      <td class="nolink">
        <a class="btn btn-warning btn-mini" title="Download job directory" href="/Jobs/<?php echo $job->getId(); ?>/Works/"><i class="icon-download-alt icon-white"></i></a>
        <a class="btn btn-danger btn-mini" title="Remove job" href="/Jobs/<?php echo $job->getId(); ?>/Remove/"><i class="icon-remove icon-white"></i></a>
      </td>
    </tr><?php
            break;
        default:
?>
    <tr class="warning">
      <td><a href="/Jobs/<?php echo $job->getId(); ?>/"></a></td>
      <td class="nolink"><a class="btn btn-mini btn-round btn-warning" title="Update state" onClick="refreshState(this);"><i class="icon-refresh icon-white"></i></a></td>
      <td class="job-title"><?php echo $job->getName(); ?></td>
      <td><?php echo $job->getStartTime()->format('Y-m-d<\b\r>H:i:s'); ?></td>
      <td><?php echo ($job->getCompleteTime() <= $job->getStartTime() ? "????-??-??<br>??:??:??" : $job->getCompleteTime()->format('Y-m-d<\b\r>H:i:s')); ?></td>
      <td class="nolink">
        <a class="btn btn-mini disabled"><i class="icon-download-alt"></i></a>
        <a class="btn btn-danger btn-mini" title="Stop job execution" href="/Jobs/<?php echo $job->getId(); ?>/Stop/"><i class="icon-stop icon-white"></i></a>
      </td>
    </tr><?php
    } // switch
} // foreach
?>
  </tbody>
</table>

<div class="btn-group"><?php
for ($i = $viewData['job_page_count']; $i > 0; $i--)
{
    if ($i == $viewData['page_number'])
    {
?>
  <a class="btn btn-small btn-success disabled"><?php echo $i; ?></a>
<?php
    }
    elseif ($i == $viewData['job_page_count'])
    {
?>
  <a class="btn btn-small" href="/Jobs/"><?php echo $i; ?></a>
<?php
    }
    else // <a class="btn btn-small disabled"><strong>. . .</strong></a>
    {
?>
  <a class="btn btn-small" href="/Jobs/Page_<?php echo $i ?>/"><?php echo $i; ?></a>
<?php
    }
}
?></div>
    <script>
      function refreshAll()
      {
          var stateIndex = 1;
          var tableRows = $('#jobs-table > tbody > tr');
          tableRows.each(function(rowIndex) {
              if ($(tableRows[rowIndex]).is('.warning'))
              {
                  refreshState(tableRows[rowIndex].cells[stateIndex].firstChild);
              }
          });
      }

      function setState(row, state, completeTime)
      {
          $(row).removeClass("warning").addClass(state == "Success" ? "success" : "error");

          var stateButton = $($(row).children()[1]).find("a");
          $(stateButton).attr('title', state == "Success" ? "Success" : "Error")
              .removeClass("btn-warning").addClass("btn-" + (state == "Success" ? "success" : "danger") + " disabled");
          $(stateButton).find("i").removeClass("icon-refresh").addClass("icon-" + (state == "Success" ? "ok" : "remove"));

          var finishTime = $(row).children()[4];
          $(finishTime).html(completeTime);

          var href = $($(row).children()[0]).find("a").attr("href");

          var actions = $($(row).children()[5]).children();
          $(actions[0]).addClass("btn-" + (state == "Success" ? "success" : "warning")).removeClass("disabled").find("i").addClass("icon-white");
          $(actions[0]).attr("title", (state == "Success" ? "Download job results" : "warning"));
          $(actions[0]).attr('href', href + (state == "Success" ? "results" : "results"));

          $(actions[1]).attr('href', href + "remove/");
          $(actions[1]).find("i").removeClass("icon-stop").addClass("icon-remove");
          $(actions[1]).attr("title", "Remove job");

          $(stateButton).attr('onClick', "");
      }

      function refreshState(stateButton)
      {
          $(stateButton).attr("onClick", "").addClass("disabled").attr("title", "Updating...");

          var response = null;
          var hasResponse = false;

          var row = $(stateButton).parent().parent();
          var href = $($(row).children()[0]).find("a").attr("href");

          $.ajax({type: "POST",
                  url: href + "update_state/",
                  success: function(data)
                  {
                      response = $.parseJSON(data);
                      hasResponse = true;
                  }
              });

          var rotate180Deg = function(element, repeats) {
              repeats--;
              $(element).animate({ borderSpacing: 180 }, {
                  step: function(now, fx) {
                      $(this).css('-webkit-transform','rotate(' + now + 'deg)');
                      $(this).css('-moz-transform','rotate(' + now + 'deg)'); 
                      $(this).css('transform','rotate(' + now + 'deg)');
                  },
                  complete: function() {
                      $(element).css('border-spacing', "0px 0px");
                      $(this).css('-webkit-transform', 'rotate(0deg)');
                      $(this).css('-moz-transform', 'rotate(0deg)'); 
                      $(this).css('transform', 'rotate(0deg)');
                      if (!hasResponse && repeats > 0) {
                          rotate180Deg(element, repeats);
                      } else if (hasResponse) {
                          if (response.status == "Updated")
                          {
                              $(stateButton).find("i").css('border-spacing', "0px 0px");
                              $(this).css('-webkit-transform','rotate(0deg)');
                              $(this).css('-moz-transform','rotate(0deg)'); 
                              $(this).css('transform','rotate(0deg)');
                              if (response.state == "SUCCESSFULL")
                              {
                                  setState(row, 'Success', response.completeTime);
                              }
                              else
                              {
                                  setState(row, 'Error', response.completeTime);
                              }
                          }
                          else
                          {
                              $(stateButton).attr("onClick", "refreshState(this);").removeClass("disabled").attr("title", "Update state");
                          }
                      } else {
                          $('#MessageBoxTitle').text("Error");
                          $('#MessageBoxBody').text("Refresh timeout.");
                          $('#MessageBox').modal();
                          $(stateButton).attr("onClick", "refreshState(this);").removeClass("disabled").attr("title", "Update state");
                      }
                  }, duration: "slow"
              }, "linear");
          };

          rotate180Deg($(stateButton).find("i"), 20);
      }

      function validate_form(form)
      {
          return false;
      }
    </script>
