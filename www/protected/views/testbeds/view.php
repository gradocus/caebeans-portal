    <style type="text/css">
      .param-group-title {
        margin-left: 0px;
        text-align: left;
        border-bottom: 2px solid #c3c3c3;
      }
      .param {
        text-align: left;
        padding-top: 15px;
        margin-left: 0px;
        border-bottom: 1px dotted #c3c3c3;
      }
      .param-value {
        text-align: right;
      }
      a.action-link {
        cursor: pointer;
        text-decoration: none;
        border-bottom: 1px dotted;
      }
      .submit-buttons {
        padding-top: 15px;
      }
      .param-value input.input-number {
        text-align: right;
      }

      .form-horizontal {
        text-align: left;
      }
      .error-message {
        visibility: hidden;
      }
    </style>

<h4>
  <ul class="nav nav-tabs">
    <li class="nav-tabs-header">Testbed: <?php echo $viewData['testbed']->getName(); ?></li>
  </ul>
</h4>

<div class="row param-group-title">
  <h5>Идентификационные параметры задачи:</h5>
</div>

<div class="row param">
  <div class="span5 param-name">
    <label class="control-label" for="inputJobName">Название задачи</label>
    <span class="help-block">Название задачи, которое будет отображаться в менеджере задач.</span>
  </div>
  <div class="span3 param-value">
    <div class="controls">
      <input type="text" id="input_user_task_name" class="span3" name="user_task_name" placeholder="Название задачи" value="<?php echo $viewData['testbed']->getName(); ?>" onChange="updateValue(this)">
      <span class="help-block">Default value: <a class="action-link" onClick="$('#input_user_task_name').val('<?php echo $viewData['testbed']->getName(); ?>');"><?php echo $viewData['testbed']->getName(); ?></a></span>
    </div>
  </div>
</div>

<div class="row param-group-title">
  <h5>Необходимые ресурсы:</h5>
</div>

<div class="row param">
  <div class="span5 param-name">
    <label class="control-label" for="input_cpu_count">Количество ядер</label>
    <span class="help-block">Необходимое количество процессорных ядер для постановки задачи.</span>
  </div>
  <div class="span3 param-value">
    <div class="controls">
      <div class="input-append">
        <input type="text" id="input_cpu_count" class="span2 input-number" name="cpu_count" placeholder="Количество ядер" value="2" onChange="updateValue(this)">
        <span class="add-on">Шт.</span>
      </div>
      <span class="help-block">Default value:: <a class="action-link" onClick="$('#input_cpu_count').val(2);">2 Шт.</a></span>
    </div>
  </div>
</div>

<div class="row param">
  <div class="span5 param-name">
    <label class="control-label" for="input_memory_size">Объем ОЗУ</label>
    <span class="help-block">Объем оперативной памяти, необходимый для постановки задачи.</span>
  </div>
  <div class="span3 param-value">
    <div class="controls">
      <div class="input-append">
        <input class="span2 input-number" type="text" id="input_memory_size" name="memory_size" placeholder="Объем ОЗУ" value="256" onChange="updateValue(this)">
        <span class="add-on">МБ</span>
      </div>
      <span class="help-block">Default value: <a class="action-link" onClick="$('#input_memory_size').val(256);">256 МБ</a></span>
    </div>
  </div>
</div>
<?php
$userHasTestbed = $viewData['testbed']->getUsers()->contains(WebApp::$User->getEntity());

foreach ($viewData['testbed']->getCategories() as $category)
{
?>
<div class="row param-group-title">
  <h5><?php echo $category->getTitle(); ?>:</h5>
</div>
<?php
    foreach ($category->getParameters() as $parameter)
    {
        if (!$parameter->isVisible())
            continue;
        $parameterName = $category->getName()."_".$parameter->getName();

        if ($parameter->getType() == "String")
            $inputClassString = "span3";
        elseif ($parameter->getType() != "File")
            $inputClassString = "span2 input-number";

        $parameterEnums = $parameter->getParameterEnums();
?>
<div class="row param">
  <div class="span5 param-name">
    <label class="control-label" for="input_<?php echo $parameterName; ?>"><?php echo $parameter->getTitle(); ?></label>
    <span class="help-block"><?php echo $parameter->getComment(); ?></span>
  </div>
  <div class="span3 param-value">
    <div class="controls">
<?php
        $hasUnits = $parameter->getUnits() != "";
        if ($hasUnits)
        {
?>
      <div class="input-append">
<?php
        }

        if ($parameter->getType() == "File")
        {
            if ($userHasTestbed)
            {
?>
        <form action="/upload/" method="post" enctype="multipart/form-data">
          <input type="hidden" name="testbed" value="<?php echo $viewData['testbed']->getId(); ?>" />
          <input type="file" name="<?php echo $parameterName; ?>" style="display: none;" onChange="sendFileAsync(this)" />
<?php
            }
            $uploadedFiles = $viewData['testbed']->getUploadedFiles();
            $uploadedFile = null;
            foreach ($uploadedFiles as $_uploadedFile)
            {
                if ($_uploadedFile->getUser() == WebApp::$User->getEntity())
                    $uploadedFile = $_uploadedFile;
            }
?>
          <div id="choice_<?php echo $parameterName; ?>"<?php echo ($uploadedFile == null ? "" : " style=\"display: none;\""); ?>>
            <a class="btn btn-small" onClick="$('input[name=<?php echo $parameterName; ?>]').click();">Select file</a>
          </div>
          <div id="upload_<?php echo $parameterName; ?>" style="display: none;">
            <div class="progress progress-success progress-striped">
              <div id="progress_<?php echo $parameterName; ?>" class="bar" style="width: 0%"></div>
            </div>
            <span id="progress_help_<?php echo $parameterName; ?>" class="help-block"></span>
          </div>
          <div id="uploaded_<?php echo $parameterName; ?>" class="input-append"<?php echo ($uploadedFile != null ? "" : " style=\"display: none;\""); ?>>
            <span id="uploaded_file_name_<?php echo $parameterName; ?>" class="input-large uneditable-input" style="text-align: left;"><?php echo ($uploadedFile != null ? $uploadedFile->getUserFileName() : ""); ?></span>
            <button class="btn" type="button" onClick="removeFileAsync('<?php echo $parameterName; ?>');">x</button>
          </div>
<?php
            if ($userHasTestbed)
            {
?>
        </form>
<?php
            }
        }
        elseif (count($parameterEnums))
        {
?>
        <select id="input_<?php echo $parameterName; ?>" class="<?php echo $inputClassString; ?>" name="<?php echo $parameterName; ?>" placeholder="<?php echo $parameter->getTitle(); ?>">
<?php
            foreach ($parameterEnums as $parameterEnum)
            {
                if ($parameterEnum->getValue() == $parameter->getValue())
                {
?>
          <option selected><?php echo $parameter->getValue(); ?></option>
<?php
                }
                else
                {
?>
          <option><?php echo $parameterEnum->getValue(); ?></option>
<?php
                }
            }
?>
        </select>
<?php
        }
        else
        {
?>
        <input class="<?php echo $inputClassString; ?>" type="text" id="input_<?php echo $parameterName; ?>" name="<?php echo $parameterName; ?>" placeholder="<?php echo $parameter->getTitle(); ?>" value="<?php echo $parameter->getValue(); ?>" onChange="updateValue(this)">
<?php
         }
         if ($hasUnits)
         {
?>
        <span class="add-on"><?php echo $parameter->getUnits(); ?></span>
      </div>
<?php
         }
         if ($parameter->getType() != "File")
         {
?>
      <span class="help-block">Default value: <a class="action-link" onClick="$('#input_<?php echo $parameterName; ?>').val('<?php echo $parameter->getValue(); ?>');"><?php echo $parameter->getValue(); if ($hasUnits) echo " ", $parameter->getUnits(); ?></a></span>
<?php
         }
?>
    </div>
  </div>
</div>
<?php
    }
}
?>
<div class="row submit-buttons">
<?php
if ($userHasTestbed && $viewData['testbed']->getState() == "AVAILABLE")
{
?>
  <form class="pull-center" action="/Testbeds/<?php echo $viewData['testbed']->getId(); ?>/submit/" method="post">
    <input id="user_task_name" type="hidden" name="user_task_name" value="<?php echo $viewData['testbed']->getName(); ?>" />
    <input id="cpu_count" type="hidden" name="cpu_count" value="2" />
    <input id="memory_size" type="hidden" name="memory_size" value="256" />
<?php
    foreach ($viewData['testbed']->getCategories() as $category)
    {
        foreach ($category->getParameters() as $parameter)
        {
            if ($parameter->isVisible())
            {
                $parameterName = $category->getName()."_".$parameter->getName();
                if ($parameter->getType() == "File" && $uploadedFile != null)
                    $parameterValue = $uploadedFile->getFileName();
                else
                    $parameterValue = $parameter->getValue();
?>
    <input id="<?php echo $parameterName; ?>" type="hidden" name="<?php echo $parameterName; ?>" value="<?php echo $parameterValue; ?>" />
<?php
            }
        }
    }
?>
    <button type="submit" class="btn btn-success">Submit</button>
<?php } elseif ($viewData['testbed']->getState() == "AVAILABLE" ) {
            if (count(WebApp::$EntityManager->getRepository('RequestForTestbed')->findBy(array('user' => WebApp::$User->getEntity()->getId(), 'testbed' => $viewData['testbed']->getId()))) == 1) { ?>
    <a class="btn btn-warning disabled" title="Already requested">Request</a>
<?php       } else { ?>
    <a class="btn btn-warning" href="/testbeds/<?php echo $viewData['testbed']->getId(); ?>/request/" title="Request for testbed">Request</a>
<?php       }
      } elseif ($userHasTestbed) { ?>
    <a class="btn btn-success disabled" title="Suspended">Submit</a>
<?php } else { ?>
    <a class="btn btn-warning disabled" title="Suspended">Request</a>
<?php } ?>
    <a class="btn" href="javascript:history.back();">Back</a>
  </form>
</div>

<script>
      function sendFileAsync(inputFile)
      {
          var inputFileName = inputFile.getAttribute("name");
          $('#choice_' + inputFileName).css('display', 'none');
          var inputFilePathParts = inputFile.value.split(/[\/\\]+/);
          $('#progress_help_' + inputFileName).html("Uploading " + inputFilePathParts[inputFilePathParts.length - 1]);
          $('#upload_' + inputFileName).css('display', 'block');

          if (window.FormData && inputFile.value != "")
          {
              var data = new FormData(inputFile.form);
              var xhr = new XMLHttpRequest();
              var url = inputFile.form.getAttribute('action') + "?time=" + (new Date()).getTime();

              var progress = $('#progress_' + inputFileName);
              progress.css('width', "0%");
              xhr.upload.addEventListener('progress', function(event) {
                  if (event.lengthComputable) {
                      progress.css('width', Math.round((event.loaded * 100) / event.total) + "%");
                  }
              }, false);

              xhr.open('post', url);
              xhr.onreadystatechange = function() {
                  if (xhr.readyState == 4 && xhr.status == 200) {
                      var result = jQuery.parseJSON(xhr.responseText);
                      if (result.status == "Uploaded") {
                          $("#" + inputFileName).val(result.fileName);
                          $('#upload_' + inputFileName).css('display', 'none');
                          $('#uploaded_file_name_' + inputFileName).html(inputFilePathParts[inputFilePathParts.length - 1]);
                          $('#uploaded_' + inputFileName).css('display', 'block');
                      } else {
                          $('#choice_' + inputFileName).css('display', 'block');
                          $('#upload_' + inputFileName).css('display', 'none');
                          $("#" + inputFileName).val("");

                          $('#MessageBoxTitle').text("Error");
                          $('#MessageBoxBody').text("File has not uploaded.");
                          $('#MessageBox').modal();
                      }
                  }
              };
              xhr.send(data);
          } else {
              $('#choice_' + inputFileName).css('display', 'block');
              $('#upload_' + inputFileName).css('display', 'none');
          }
      }
      function removeFileAsync(inputFileName)
      {
          $.post("/Testbeds/<?php echo $viewData['testbed']->getId(); ?>/RemoveFile/" + "?time=" + (new Date()).getTime(),
              { fileName: $('#' + inputFileName).val() },
          function(response) {
              var result = jQuery.parseJSON(response);
              if (result.status == "Removed")
              {
                  $('#choice_' + inputFileName).css('display', 'block');
                  $('#uploaded_' + inputFileName).css('display', 'none');
                  $("#" + inputFileName).val("");
              }
              else
              {
                  $('#MessageBoxTitle').text("Error");
                  $('#MessageBoxBody').text("File: " + result.error);
                  $('#MessageBox').modal();
              }
          });
      }
      function updateValue(inputElement)
      {
          var inputName = inputElement.getAttribute("name");
          $("#" + inputName).val(inputElement.value);
      }
    </script>
