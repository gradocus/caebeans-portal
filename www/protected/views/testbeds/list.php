    <style type="text/css">
      .table th, .table td {
        text-align: center;
        vertical-align: middle;
      }
      .table th:first-child, .table td:first-child {
        width: 50%;
        padding-left: 15px;
        text-align: left;
      }
      tr.unavailable {
        color: #999999;
      }
      .rowlink > td {
        cursor: pointer;
      }
      .rowlink > td.nolink {
        cursor: auto;
      }
   </style>

        <h4>
          <ul class="nav nav-tabs">
            <li class="nav-tabs-header">Testbeds:</li>
<?php
if ($viewData['allButMyIsActive'])
{
?>
            <li><a href="#my-testbeds" data-toggle="tab">My Testbeds</a></li>
            <li class="active"><a href="#all-but-my-testbeds" data-toggle="tab">All Testbeds</a></li>
<?php
} else {
?>
            <li class="active"><a href="#my-testbeds" data-toggle="tab">My Testbeds</a></li>
            <li><a href="#all-but-my-testbeds" data-toggle="tab">All Testbeds</a></li>
<?php
}
?>
          </ul>
        </h4>

        <div class="tab-content">
          <div class="tab-pane<?php if ($viewData['allButMyIsActive']) echo " active"; ?>" id="all-but-my-testbeds">
            <table class="table table-condensed table-striped table-hover">
              <thead>
                <tr>
                  <th>Testbed name</th>
                  <th>Author</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="rowlink" data-provides="rowlink">
<?php
foreach ($viewData['testbeds'] as $testbed)
{
    $requestedTestbed = false;

    foreach (WebApp::$User->getEntity()->getRequestsForTestbeds() as $request)
        $requestedTestbed |= $request->getTestbed() == $testbed;

    if ($testbed->getState() === 'AVAILABLE')
    {
?>
                <tr>
                  <?php if ($requestedTestbed): ?>
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>) <span class="label label-info">requested</span></a></td>
                  <?php else: ?>
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</a></td>
                  <?php endif; ?>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td class="nolink">
<!--                    <a class="btn btn-mini" title="View testbed" href="/testbeds/<?php echo $testbed->getId(); ?>/"><i class="icon-list-alt"></i></a>-->
                    <?php if ($requestedTestbed): ?>
                    <a class="btn btn-warning btn-mini disabled" title="Already requested"><i class="icon-share icon-white"></i></a>
                    <?php else: ?>
                    <a class="btn btn-warning btn-mini" title="Request for testbed" href="/testbeds/<?php echo $testbed->getId(); ?>/request.json" onClick="request(this); return false;"><i class="icon-share icon-white"></i></a>
                    <?php endif; ?>
                  </td>
                </tr>
<?php
    }
    elseif ($testbed->getState() === 'SUSPENDED')
    {
?>
                <tr class="unavailable">
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>) <span class="label">unavailable</span></a></td>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td class="nolink">
<!--                    <a class="btn btn-mini" title="View testbed" href="/testbeds/<?php echo $testbed->getId(); ?>/"><i class="icon-list-alt"></i></a>-->
                    <a class="btn btn-warning btn-mini disabled" title="Suspended"><i class="icon-share icon-white"></i></a>
                  </td>
                </tr>
<?php
    } // else $testbed->getState() === 'HIDDEN';
}
?>
              </tbody>
            </table>
<div class="btn-group"><?php
for ($i = $viewData['testbed_page_count']; $i > 0; $i--)
{
    if ($i == $viewData['page_number'])
    {
?>
  <a class="btn btn-small btn-success disabled"><?php echo $i; ?></a>
<?php
    }/*
    elseif ($i == $viewData['testbed_page_count'])
    {
?>
  <a class="btn btn-small" href="/testbeds/"><?php echo $i; ?></a>
<?php
    }*/
    else // <a class="btn btn-small disabled"><strong>. . .</strong></a>
    {
?>
  <a class="btn btn-small" href="/testbeds/page_<?php echo $i ?>/"><?php echo $i; ?></a>
<?php
    }
}
?></div>

          </div>
          <div class="tab-pane <?php if (!$viewData['allButMyIsActive']) echo " active"; ?>" id="my-testbeds">
            <table class="table table-condensed table-striped table-hover">
              <thead>
                <tr>
                  <th>Testbed name</th>
                  <th>Author</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="rowlink" data-provides="rowlink">
<?php
foreach ($viewData['myTestbeds'] as $testbed)
{
?>
<?php if ($testbed->getState() == "AVAILABLE"): ?>
                <tr>
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>)</a></td>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td class="nolink">
                    <a class="btn btn-mini" title="View testbed jobs" href="/testbeds/<?php echo $testbed->getId(); ?>/jobs/"><i class="icon-list-alt"></i></a>
                    <a class="btn btn-danger btn-mini" title="Unsubscribe" href="/testbeds/<?php echo $testbed->getId(); ?>/unsubscribe/" onClick="unsubscribe(this); return false;"><i class="icon-ban-circle icon-white"></i></a>
                  </td>
                </tr>
<?php elseif ($testbed->getState() == "SUSPENDED"): ?>
                <tr class="unavailable">
                  <td><a href="/testbeds/<?php echo $testbed->getId(); ?>/"><?php echo $testbed->getName(); ?> (<?php echo $testbed->getVersion(); ?>) <span class="label">unavailable</span></a></td>
                  <td><?php echo $testbed->getAuthor(); ?></td>
                  <td class="nolink">
                    <a class="btn btn-mini" title="View testbed jobs" href="/testbeds/<?php echo $testbed->getId(); ?>/jobs/"><i class="icon-list-alt"></i></a>
                    <a class="btn btn-danger btn-mini" title="Unsubscribe" href="/testbeds/<?php echo $testbed->getId(); ?>/unsubscribe/" onClick="unsubscribe(this); return false;"><i class="icon-ban-circle icon-white"></i></a>
                  </td>
                </tr>
<?php endif; ?>

<?php
}
?>
              </tbody>
            </table>
          </div>
        </div>

        <script>
          function request(requestButton)
          {
              $(requestButton).attr("onClick", "return false;").addClass("disabled").attr("title", "Sending request...");
              var testbedRow = $(requestButton).parent().parent();
              var requestLink = $(requestButton).attr("href");

              $.ajax({type: "GET",
                      url: requestLink
                  }).done(function(data) {
                      var response = $.parseJSON(data);
                      if (response.result == "error")
                      {
                          $('#MessageBoxTitle').text("Error");
                          $('#MessageBoxBody').text(response.error);
                          $('#MessageBox').modal();
                          $(requestButton).attr("onClick", "request(this); return false;").removeClass("disabled").attr("title", "Request for testbed");
                      }
                      else
                      {
                          $(requestButton).attr("title", "Already requested");
                          $(testbedRow).find("td:first").html($(testbedRow).find("td:first").html() + ' <span class="label label-info">requested</span>');
                      }
                  }).fail(function(xhr) {
                      $('#MessageBoxTitle').text("Error");
                      $('#MessageBoxBody').text("Что-то пошло не так. Повторите Ваш запрос еще раз.");
                      $('#MessageBox').modal();
                      $(requestButton).attr("onClick", "request(this); return false;").removeClass("disabled").attr("title", "Request for testbed");
                  });
          }

          function unsubscribe(unsubscribeButton)
          {
              var testbedRow = $(unsubscribeButton).parent().parent();
              var unsubscribeLink = $(unsubscribeButton).attr("href");

              var testbedName = $(testbedRow).find("td:first").text();
              var answer = confirm("A you sure want to unsubscribe from \"" + testbedName + "\" testbed?");
              if (!answer)
                  return false;
              else
                  $(unsubscribeButton).attr("onClick", "return false;").addClass("disabled").attr("title", "Unsubscribing...");

              $.ajax({type: "GET",
                      url: unsubscribeLink
                  }).done(function(data) {
                      var response = $.parseJSON(data);
                      if (response.result == "error")
                      {
                          $('#MessageBoxTitle').text("Error");
                          $('#MessageBoxBody').text(response.error);
                          $('#MessageBox').modal();
                          $(unsubscribeButton).attr("onClick", "unsubscribe(this); return false;").removeClass("disabled").attr("title", "Unsubscribe");
                      }
                      else
                      {
//                          $(testbedRow).remove();
                          window.location = "/testbeds/";
                      }
                  }).fail(function(xhr) {
                      $('#MessageBoxTitle').text("Error");
                      $('#MessageBoxBody').text("Что-то пошло не так. Повторите Ваш запрос еще раз.");
                      $('#MessageBox').modal();
                      $(unsubscribeButton).attr("onClick", "unsubscribe(this); return false;").removeClass("disabled").attr("title", "Unsubscribe");
                  });
          }
        </script>
