<h4>Sign in</h4>

        <hr>

        <form class="form-horizontal" action="" method="post">
          <div class="control-group">
            <label class="control-label" for="inputEmail">Email</label>
            <div class="controls">
              <input type="text" id="inputEmail" name="login" placeholder="Email" value="<?php echo $viewData['loginForm']->login; ?>">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="inputPassword">Password</label>
            <div class="controls">
              <input type="password" id="inputPassword" name="password" placeholder="Password">
              <span class="help-inline error-message">Something may have gone wrong</span>
            </div>
          </div>
          <div class="control-group">
            <div class="controls">
              <label class="checkbox">
                <input type="checkbox" name="rememberMe"> Remember me
              </label>
            </div>
          </div>
<?php if($viewData['loginForm']->message != ''): ?>
          <div class="control-group">
            <div class="controls">
              <div class="alert alert-error">
                <?php echo $viewData['loginForm']->message; ?>
              </div>
            </div>
          </div>
<?php endif; ?>
          <div class="control-group">
            <div class="controls">
              <button type="submit" class="btn btn-primary" name="LoginForm" value="Submit">Sign in</button>
              <a class="forgot" href="#forgot-your-password">Forgot your password?</a>
            </div>
          </div>
        </form>
