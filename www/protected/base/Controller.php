<?php
defined('WEB_APP') or die("403");

require_once WEB_APP_PATH."base/View.php";

abstract class Controller
{
    public function __construct(Array $actions = array())
    {
        $actionsString = print_r($actions, true);
        Debug::writeLine("Controller->__construct(actions:\"".$actionsString."\")");
        $this->actions = $actions;
    }

    protected function doActionProtection()
    {
        // Add protection code for each controller here...
    }

    private $actions;

    // The default action for a controller.
    abstract protected function defaultAction();

    final public function doAction($request = null)
    {
        $this->doActionProtection();

        $request = ($request === null) ? "" : trim($request);
        Debug::writeLine("Controller->doAction(request:\"".$request."\")"); Debug::levelUp();
        if ($request === "")
        {
            Debug::writeLine("Do defaultAction");
            $this->defaultAction();
        }
        elseif (count($this->actions) > 0)
        {
            Debug::writeLine("Search action in \"action array\""); Debug::levelUp();
            $found = false;
            foreach ($this->actions as $regexp => $methodName)
            {
                if (preg_match($regexp, $request, $matches) == 1
                    && method_exists($this, $methodName))
                {
                    Debug::writeLine("Call method \"$methodName\"");
                    $found = true;
                    $this->$methodName(array_slice($matches, 1));
                    break;
                }
            }
            if (!$found)
            {
                Debug::writeLine("Method not found");
                throw new Exception("Bad request \"$request\"");
            }
            Debug::levelDown();
        }
        else
        {
            Debug::writeLine("Action array is empty");
            throw new Exception("Bad request \"$request\"");
        }
        Debug::levelDown();
    }

    protected $view;
}
?>
