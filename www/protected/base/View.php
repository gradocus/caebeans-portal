<?php
defined('WEB_APP') or die("403");

class View Implements ArrayAccess
{
    public function __construct($page = "home/view", $layout = "main")
    {
        $this->layout = $layout;
        $this->page = $page;
    }

    // --------------------------------------------
    protected $layout;

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($newLayout)
    {
        $this->layout = $newLayout;
    }

    protected $page = null;

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($newPage)
    {
        $this->page = $newPage;
    }

    // --------------------------------------------
    private $viewData = array();

    function offsetExists($offset)
    {
        return isset($this->viewData[$offset]);
    }

    function offsetGet($offset)
    {
        return $this->viewData[$offset];
    }

    function offsetSet($offset, $value)
    {
        $this->viewData[$offset] = $value;
    }

    function offsetUnset($offset)
    {
        unset($this->viewData[$offset]);
    }
    // --------------------------------------------

    public function printPageSource()
    {
        $viewData = &$this->viewData;

        $contentScript = WEB_APP_PATH."views/".$this->page.".php";
        include WEB_APP_PATH."layouts/".$this->layout.".php";
    }
}
?>
