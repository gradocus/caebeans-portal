<?php

define('WEB_APP_CONFIG_PATH', WEB_APP_PATH."config/");

return array
    (
        'name' => "Web application",

        'db' => require(WEB_APP_CONFIG_PATH."database.php"),

        'doctrine' => require(WEB_APP_CONFIG_PATH."doctrine.php"),

        'route' => array
            (
                'type' => "Rewrite",

                'defaultController' => "Home"
            ),

        'application' => require(WEB_APP_CONFIG_PATH."application.php"),

        'controllers' => require(WEB_APP_CONFIG_PATH."controllers.php"),

		'caeserver' => require(WEB_APP_CONFIG_PATH."caeserver.php"),

        'log' => array
            (                                           //
                'type' => "filesystem",                 // "database" or "filesystem". Default value: "filesystem".
                                                        // If log type is "filesystem" then you can set location:
                'location' => "framework",              // "application" or "framework". Default value: "framework".
                                                        //
                'levels' => array("error", "warning"),  // "debug", "error" and/or "warning". Default values: "error, warning".
                                                        // The debug level will be write all available messages into debug-file.
            ),

        'version' => "0.1",
    );
?>
