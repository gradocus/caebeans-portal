<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>DiVTB Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="wrapper">
          <a class="brand" href="/">DiVTB Portal</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="/">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
            <ul class="nav pull-right">
              <li><button class="btn btn-success" onClick="window.location='/account/signup/';">Sign up</button></li>
              <li><a href="/account/signin/">Sign in</a></li>
            </ul>
          </div><!-- ./nav-collapse -->
        </div><!-- ./wrapper -->
      </div><!-- ./navbar-inner -->
    </div><!-- ./navbar -->

    <div class="wrapper">

      <div class="content">
<?php include $contentScript; ?>
      </div>

    </div><!-- ./wrapper -->
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  </body>
</html>
