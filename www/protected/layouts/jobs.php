<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $viewData['pageTitle']; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">

    <link href="/css/style.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="wrapper">
          <a class="brand" href="/"><?php echo WebApp::$Properties['application']['title']; ?></a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="active"><a href="/jobs/">Jobs</a></li>
              <li><a href="/testbeds/">Testbeds</a></li>
            </ul>
            <ul class="nav pull-right">
              <li><div class="btn-group">
                <button class="btn dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo WebApp::$User->getEntity()->getFirstName(), " ", WebApp::$User->getEntity()->getMiddleName(), " ",  WebApp::$User->getEntity()->getLastName(); ?> <span class="caret caret-black"></span></button>
                <ul class="dropdown-menu">
<?php
if (WebApp::$User->getEntity()->getName() == "Administrator")
{
?>
                  <li><a href="/admin/"><i class="icon-globe"></i> Admin panel</a></li>
                  <li class="divider"></li>
<?php
}
?>
                  <li><a href="#inbox"><i class="icon-envelope"></i> Inbox (0)</a></li>
                  <li><a href="/account/settings/"><i class="icon-wrench"></i> Account settings</a></li>
                  <li class="divider"></li>
                  <li><a href="/account/signout/"><i class="icon-off"></i> Sign out</a></li>
                </ul>
              </div><!-- /btn-group --></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.wrapper -->
      </div>
    </div>

    <div id="MessageBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="MessageBoxTitle" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="MessageBoxTitle"></h3>
      </div>
      <div id="MessageBoxBody" class="modal-body"></div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>

    <div class="wrapper">
      <div class="content">

<?php include $contentScript; ?>

      </div><!-- ./content -->
    </div><!-- ./wrapper -->

    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap-rowlink.min.js"></script>
    <script>
        $(document).ready(function() {
            refreshAll();
        });
    </script>
  </body>
</html>
