<?php

class WebAppError extends Exception
{
    public function __construct($message, $code = 0, Exception $previous = null)
    {
//        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found', true, 404);
        parent::__construct($message, $code, $previous);
    }

    public function getController()
    {
        return null;
    }
}
?>
