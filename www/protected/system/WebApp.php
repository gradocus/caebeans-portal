<?php
define('WEB_APP', true);

session_start();

defined('WEB_APP_START_TIME') or define('WEB_APP_START_TIME', microtime(true));
defined('WEB_APP_PATH') or define('WEB_APP_PATH', dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR);
// Default debugging is disabled.
defined('WEB_APP_DEBUG') or define('WEB_APP_DEBUG', false);
// Default debug level is 0 (only exceptions).
defined('WEB_APP_DEBUG_LEVEL') or define('WEB_APP_DEBUG_LEVEL', 0);

include WEB_APP_PATH."system/Debug.php";
include WEB_APP_PATH."system/Error.php";
include WEB_APP_PATH."system/Config.php";
include WEB_APP_PATH."system/WebAppUser.php";
include WEB_APP_PATH."system/Router.php";

include WEB_APP_PATH."system/WebAppError.php";

class WebApp
{
    public function __construct()
    {
        Debug::writeLine("WebApp __construct");
    }

    static public $Properties;
    static public $EntityManager;
    static public $User;

    public function loadProperties($propertiesFilePath)
    {
        Debug::writeLine("WebApp loadProperties:"); Debug::levelUp();
        WebApp::$Properties->load($propertiesFilePath);
        Debug::levelDown();
    }

    public function run()
    {
        Debug::writeLine("WebApp run:"); Debug::levelUp();
        $router = new Router();
        try
        {
            $controller = $router->getController();
            $controller->doAction($router->getRequest());
        }
        catch (PortalError $error)
        {
            $controller = $error->getController();
            $controller->doAction($error);
        }
        catch(Exception $ex)
        {
            // TODO: Add default error controllers and replace this.
            Error::writeLine($ex->getMessage());
/*
            require_once WEB_APP_PATH."controllers/Error500.php";
            $controller = new Error500();
            $controller->doAction($_SERVER["REQUEST_URI"]);
*/
        }
        Debug::levelDown();
    }
}

Debug::writeLine("WebApp static fields initialization:"); Debug::levelUp();
WebApp::$Properties = new Config();

require_once WEB_APP_PATH."doctrine/bootstrap.php";
WebApp::$EntityManager = $entityManager;

WebApp::$User = new WebAppUser();
Debug::levelDown();
?>
