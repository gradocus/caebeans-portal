<?php
defined('WEB_APP') or die("403");

class Router
{
    public function __construct()
    {
//        Debug::writeLine("Router: __construct(\"".isset($_GET['request'])?$_GET['request']:""."\")"); Debug::levelUp();
        if (!isset($_GET['request']) || trim($_GET['request']) === "")
        {
            Debug::writeLine("Setting a default controller class name.");
            $this->controllerClassName = WebApp::$Properties['route']['defaultController'];
        }
        else
        {
            Debug::writeLine("Searching a controller class name.");
            foreach (WebApp::$Properties['controllers'] as $regexp => $controllerName)
            {
                if (preg_match($regexp, $_GET['request'], $matches) == 1)
                {
                    Debug::writeLine("The controller class name found (\"$controllerName\").");
                    $this->controllerClassName = $controllerName;
                    break;
                }
            }
            $this->request = preg_replace("/".$matches[1]."\/?/", "", trim($_GET['request']), 1);
            unset($_GET['request']);
        }
        if ($this->controllerClassName === "")
        {
            Debug::writeLine("The controller class name not found - set error 404 controller as current controller.");
            $this->controllerClassName = "Error404"; //WebApp::$Resources['Properties']['route']['controller404'];
        }
        Debug::levelDown();
    }

    private $request = "";
    private $controllerClassName = "";
    private $controller = null;

    public function getController()
    {
        Debug::writeLine("Router: getController"); Debug::levelUp();
        if ($this->controller !== null)
        {
            Debug::levelDown();
            return $this->controller;
        }

        Debug::writeLine("Searching the controller class name: \"$this->controllerClassName\"");
        if (class_exists($this->controllerClassName) || file_exists(WEB_APP_PATH."controllers/".$this->controllerClassName.".php"))
        {
            require_once WEB_APP_PATH."controllers/".$this->controllerClassName.".php";
            if (class_exists($this->controllerClassName)
                && gettype($controller = new $this->controllerClassName()) === "object"
                && get_parent_class($controller) === "Controller")
            {
                $this->controller = $controller;
                Debug::levelDown();
                return $this->controller;
            }
        }

        Debug::writeLine("Bad controller name \"$this->controllerClassName\"");
        throw new Exception("Bad controller name \"$this->controllerClassName\"");
    }

    public function getRequest()
    {
        Debug::writeLine("Router: getting request: \"$this->request\"");
        return $this->request;
    }
}
?>
