<?php
defined("WEB_APP") or die("403");

class MySQL {
    static private $connected = false;
    var $query_num = 0;
    var $query_time = 0;
    var $error = "";
    var $error_num = 0;
    var $show_errors = true;
    static private $my_login = false;
    private $my_query = false;
    var $version = "";

    public function __construct() {
        $db_config = WebApp::$Properties['db'];
        $this->db_host = $db_config['hostname'];
        $this->db_name = $db_config['dbname'];
        $this->db_user_login = $db_config['username'];
        $this->db_user_password = $db_config['password'];

        MySQL::$PREFIX = ($db_config['prefix'] === "") ? "" : $db_config['prefix']."_";
    }

    private $db_host;
    private $db_name;
    private $db_user_login;
    private $db_user_password;

    public static $PREFIX = "";

    public function connect() {
        self::$my_login = @mysql_connect($this->db_host, $this->db_user_login, $this->db_user_password);
        if(!self::$my_login) {
            $this->error = mysql_error();
            $this->error_num = mysql_errno();
            if($this->show_errors) {
                $this->display_error($this->error, $this->error_num);
            }
            return false;
        }

        if (!@mysql_select_db($this->db_name, self::$my_login)) {
            $this->error = mysql_error();
            $this->error_num = mysql_errno();
            if($this->show_errors)
            {
                $this->display_error($this->error, $this->error_num);
            }
            return false;
        }

        $this->version = mysql_get_server_info();
        @mysql_query("SET NAMES utf8", self::$my_login);

        return true;
    }

    public function query($query) {
        if(!self::$connected) {
            if($this->connect()) {
                self::$connected = true;
            } else {
                return false;
            }
        }
        $start_time = $this->get_time();
        $this->my_query = @mysql_query($query);
        if(!$this->my_query) {
            $this->error = mysql_error();
            $this->error_num = mysql_errno();
            $this->my_query = "";
            if($this->show_errors) {
                $this->display_error($this->error, $this->error_num);
            }
            return false;
        }
        $this->query_time += $this->get_time() - $start_time;
        $this->query_num++;
        return true;
    }

    public function get_num_rows() {
        if( isset($this->my_query) && ($this->my_query != "") ) {
            return mysql_num_rows($this->my_query);
        } else {
            return false;
        }
    }

    public function get_row() {
        if ($this->my_query) {
            return @mysql_fetch_row($this->my_query);
        } else {
            return null;
        }
    }

    public function get_array() {
        if ($this->my_query) {
            return @mysql_fetch_array($this->my_query);
        } else {
            return null;
        }
    }

    public function free_query() {
        @mysql_free_result($this->my_query);
    }

    public function disconnect() {
        if($this->connected) {
            @mysql_close();
            $this->connected = false;
            return true;
        }
        return false;
    }

    public function display_error($error, $error_num) {
        echo '<?xml version="1.0" encoding="iso-8859-1"?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <title>MySQL Error</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <style type="text/css">
        <!--
        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 10px;
            font-style: normal;
            color: #000000;
        }
        -->
        </style>
        </head>
        <body>
            <font size="4">MySQL Error!</font>
            <br>------------------------<br>
            <br>

            <u>The Error returned was: </u>
            <br>
                <strong>'.$error.'</strong>

            <br><br>
            </strong><u>Error Number: </u>
            <br>
                <strong>'.$error_num.'</strong>
            <br>

        </body>
        </html>';

        exit();
    }

    private function get_time() {
        $time = microtime();
        $time = explode(" ",$time);
        $time = $time[1] + $time[0];
        return $time;
    }
}
?>
