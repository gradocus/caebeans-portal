<?php

require_once WEB_APP_PATH."system/WebAppError.php";

class Error404 extends WebAppError
{
    public function __construct($message, $code = 404, Exception $previous = null)
    {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found', true, 404);

        parent::__construct($message, $code, $previous);
    }
}
?>
