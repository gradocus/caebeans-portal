<?php

require_once WEB_APP_PATH."system/WebAppError.php";

class Error403 extends WebAppError
{
    public function __construct($message, $code = 403, Exception $previous = null)
    {
        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden', true, 403);

        parent::__construct($message, $code, $previous);
    }
}
?>
