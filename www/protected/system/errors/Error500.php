<?php

require_once WEB_APP_PATH."system/WebAppError.php";

class Error500 extends WebAppError
{
    public function __construct($message, $code = 500, Exception $previous = null)
    {
        header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error', true, 500);

        parent::__construct($message, $code, $previous);
    }
}
?>
