<?php
class Debug
{
    static protected $file = null;
    static protected $level = null;

    static public function levelUp()
    {
        if (WEB_APP_DEBUG === false)
            return false;

        if (Debug::$level === null)
            Debug::$level = 1;
        else
            Debug::$level++;
    }

    static public function writeLine($string = "")
    {
        if (WEB_APP_DEBUG === false || Debug::$level > WEB_APP_DEBUG_LEVEL)
            return false;

        $ident = "";
        for ($i = 0; $i < Debug::$level; $i++)
            $ident .= "   ";

        if (Debug::$file === null)
            Debug::$file = fopen(WEB_APP_PATH."debug.log", "w") or die("Can't open debug file for writing...");

        $date = new DateTime();
        fwrite(Debug::$file, $date->format("[Y.m.d H:i:s.").substr((string)microtime(true), 1, 8)."] ".$ident.$string."\r\n");
    }

    static public function levelDown()
    {
        if (WEB_APP_DEBUG === false)
            return false;

        if (Debug::$level !== null && Debug::$level !== 0)
            Debug::$level--;
    }
}
?>
