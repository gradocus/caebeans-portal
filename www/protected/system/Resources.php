<?php
defined('WEB_APP') or die("403");

class Resources Implements ArrayAccess
{
    public function __construct()
    {
        $this->resources = array();
    }

    private $resources;

    public function set($key, $value)
    {
        $lowerKey = strtolower($key);
        if (isset($this->resources[$lowerKey]))
        {
            throw new Exception("Resource with key \"{$key}\" is already exists.");
        }
        else
        {
            $this->resources[$lowerKey] = $value;
        }
    }

    public function get($key)
    {
        $lowerKey = strtolower($key);
        if (isset($this->resources[$lowerKey]))
        {
            return $this->resources[$lowerKey];
        }
        else
        {
            return null;
        }
    }

    public function remove($key)
    {
        $lowerKey = strtolower($key);
        unset($this->resources[$lowerKey]);
    }

    function offsetExists($offset)
    {
        $lowerKey = strtolower($offset);
        return isset($this->resources[$lowerKey]);
    }

    function offsetGet($offset)
    {
        return $this->get($offset);
    }

    function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    function offsetUnset($offset)
    {
        $this->remove($offset);
    }
}
?>
