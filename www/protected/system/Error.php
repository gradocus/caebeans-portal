<?php
class Error
{
    static protected $file = null;
    static protected $level = 0;

    static public function levelUp()
    {
        Error::$level++;
    }

    static public function writeLine($string = "")
    {
        $ident = "";
        for ($i = 0; $i < Error::$level; $i++)
            $ident .= "   ";

        if (Error::$file === null)
            Error::$file = fopen(WEB_APP_PATH."error.log", "a") or die("Can't open error file for appending...");

        $date = new DateTime();
        fwrite(Error::$file, sprintf("%s - [%s] %s\r\n", WebApp::$User->getIpAddress(), $date->format("Y.m.d H:i:s.").substr((string)microtime(true), 1, 8), $ident.$string));
    }

    static public function levelDown()
    {
        if (Error::$level > 0)
            Error::$level--;
    }
}
?>
