<?php
class WebAppUser
{
    public function __construct()
    {
        // Check Session.
        if (isset($_SESSION['id']))
        {
            $this->entity = WebApp::$EntityManager->find('User', (int)$_SESSION['id']);
            if ($this->entity === null)
            {
                // Warning: user with id "$_SESSION['id']" is not found.
                unset($_SESSION['id']);
            }
            else
            {
                return;
            }
        }
        // Check Cookie.
        if (isset($_COOKIE['user_id']) && isset($_COOKIE['password']))
        {
            $user = WebApp::$EntityManager->find('User', (int)$_COOKIE['user_id']);
            if ($user !== null && $user->getPassword() === $_COOKIE['password'])
            {
                $this->entity = $user;
                return;
            }
            else
            {
                // Warning: user with id "$_COOKIE['user_id']" and password "$_COOKIE['password']" is not found.
                $this->unsetCookie("user_id");
                $this->unsetCookie("password");
            }
        }
        // $this->user = User::ANONYMOUS_USER;
        // echo "ANONYMOUS_USER<br>\n";
    }

    private $entity = null;
    public function getEntity()
    {
        return $this->entity;
    }

    public function isAuthorised()
    {
        return $this->entity !== null && !$this->entity->IsBanned();
    }

    public function login($login, $password, $remember = false)
    {
        $user = WebApp::$EntityManager->getRepository('User')->findOneBy(array('email' => $login, 'password' => md5(md5($password))));
        if ($user === null)
        {
            return false;
        }

        if ($remember)
        {
            setcookie('user_id', $user->getId(), time() + 3600 * 24, '/');
            setcookie('password', $user->getPassword(), time() + 3600 * 24, '/');
        }
        else
        {
            $_SESSION['id'] = $user->getId();
        }

        $this->entity = $user;
        return true;
    }

    public function unsetCookie($cookieName)
    {
        unset($_COOKIE[$cookieName]);
        setcookie($cookieName, null, -1, '/');
    }

    public function logout()
    {
        if ($this->isAuthorised())
        {
            if (isset($_SESSION['id']))
            {
                unset($_SESSION['id']);
            }
            if (isset($_COOKIE['user_id']))
            {
                WebApp::$User->unsetCookie("user_id");
                WebApp::$User->unsetCookie("password");
            }
            $this->entity = null;
        }
    }

    public static function redirect($localPath, $comeBack = false)
    {
        if (WebApp::$Properties['route']['type'] === "Rewrite")
        {
            header("Location: /".$localPath.($comeBack ? "?go=".$_SERVER["REQUEST_URI"] : ""));
        }
        else
        {
            header("Location: /index.php?request=".$localPath.($comeBack ? "&go=".$_SERVER["REQUEST_URI"] : ""));
        }
        exit;
    }

    private static $ipAddress = null;
    public static function getIpAddress()
    {
        if (self::$ipAddress === null)
        {
            $remoteIp = $_SERVER['REMOTE_ADDR'];
            if (!$remoteIp) {
                $remoteIp = urldecode(getenv('HTTP_CLIENTIP'));
            }

            if (getenv('HTTP_X_FORWARDED_FOR')) {
                self::$ipAddress = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_X_FORWARDED')) {
                self::$ipAddress = getenv('HTTP_X_FORWARDED');
            } elseif (getenv('HTTP_FORWARDED_FOR')) {
                self::$ipAddress = getenv('HTTP_FORWARDED_FOR');
            } elseif (getenv('HTTP_FORWARDED')) {
                self::$ipAddress = getenv('HTTP_FORWARDED');
            } else {
                self::$ipAddress = $_SERVER['REMOTE_ADDR'];
            }

            if($remoteIp != self::$ipAddress){
                self::$ipAddress = $remoteIp.", ".self::$ipAddress;
            }
        }

        return self::$ipAddress;
    }
}
?>
