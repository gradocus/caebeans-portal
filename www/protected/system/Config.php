<?php
defined('WEB_APP') or die("403");

class Config Implements ArrayAccess
{
    public function __construct()
    {
        $this->defaultConfig = include WEB_APP_PATH."config.php";
        $this->defaultConfig = array_change_key_case($this->defaultConfig, CASE_LOWER);
        $this->modifiedConfig = array();
    }

    private $defaultConfig;
    private $modifiedConfig;

    public function load($pathToFile)
    {
        if (!file_exists($pathToFile))
            throw new Exception("Config file not found: ".$pathToFile);
        try
        {
            $this->modifiedConfig = include $pathToFile;
            $this->modifiedConfig = array_change_key_case($this->modifiedConfig, CASE_LOWER);
            return true;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }

    function offsetExists($offset)
    {
        $lowerKey = strtolower($offset);
        return isset($this->modifiedConfig[$lowerKey]) || isset($this->defaultConfig[$lowerKey]);
    }

    function offsetGet($offset)
    {
        $lowerKey = strtolower($offset);
        if (isset($this->modifiedConfig[$lowerKey]))
            return $this->modifiedConfig[$lowerKey];
        elseif (isset($this->defaultConfig[$lowerKey]))
            return $this->defaultConfig[$lowerKey];
        else
            return null;
    }

    function offsetSet($offset, $value)
    {
        $lowerKey = strtolower($offset);
        $this->modifiedConfig[$lowerKey] = $value;
    }

    function offsetUnset($offset)
    {
        $lowerKey = strtolower($offset);
        unset($this->modifiedConfig[$lowerKey]);
    }
}
?>
