<?php
return array
    (
        'route' => array
            (
                'type' => "Rewrite",

                'defaultController' => "Upload"
            ),

        'controllers' => array
            (
                '/^(upload)/i' => "Upload",
            ),
    );
?>
