#!/bin/bash
SCRIPTPATH=`dirname ${BASH_SOURCE[0]}`
cd $SCRIPTPATH

../../../composer/vendor/bin/doctrine orm:schema-tool:drop --force
rm sqlite/*.sqlite
