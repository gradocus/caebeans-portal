<?php
define('DOCTRINE', true);

class_exists('WebApp') or require_once "../system/WebApp.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// Include Composer Autoload (relative to project root).
require_once WebApp::$Properties['doctrine']['path_to_repository']."/vendor/autoload.php";

$paths = array(__DIR__."/config/xml");
$isDevMode = false;

// the connection configuration
$conn = array(
    'driver' => 'pdo_sqlite',
    'path' => __DIR__."/sqlite/database.sqlite",
);

$config = Setup::createXMLMetadataConfiguration($paths, $isDevMode);
$config->setProxyDir(WEB_APP_PATH."doctrine/proxies");
$config->setProxyNamespace('Portal\Proxies');
$config->setAutoGenerateProxyClasses(true);
$entityManager = EntityManager::create($conn, $config);


require_once __DIR__."/entities/User.php";
require_once __DIR__."/entities/Group.php";

require_once __DIR__."/entities/ParameterEnum.php";
require_once __DIR__."/entities/Parameter.php";
require_once __DIR__."/entities/Category.php";
require_once __DIR__."/entities/Testbed.php";
require_once __DIR__."/entities/RequestForTestbed.php";

require_once __DIR__."/entities/UploadedFile.php";

require_once __DIR__."/entities/ParameterValue.php";
require_once __DIR__."/entities/Job.php";
/*
if (!class_exists("Doctrine\Common\Version", false)) {
    require_once "bootstrap_doctrine.php";
}
*/
require_once __DIR__."/repositories/UserRepository.php";
require_once __DIR__."/repositories/TestbedRepository.php";
require_once __DIR__."/repositories/JobRepository.php";

?>
