<?php
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function getRecentUsers($number = 30)
    {
        $dql = "SELECT u FROM User u ORDER BY u.id DESC";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setMaxResults($number);
        return $query->getResult();
    }
}
?>
