<?php
use Doctrine\ORM\EntityRepository;

class TestbedRepository extends EntityRepository
{
    public function getRecentTestbeds($number = 10)
    {
        $dql = "SELECT t FROM Testbed t ORDER BY t.id DESC";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setMaxResults($number);
        return $query->getResult();
    }

    public function getTestbedPageCount($perPage = 10)
    {
        $dql = "SELECT count(t.id) FROM Testbed t WHERE t NOT IN(SELECT t2 FROM Testbed t2 JOIN t2.users u WHERE u.id='".WebApp::$User->getEntity()->getId()."')";
        $query = $this->getEntityManager()->createQuery($dql);
        $count = $query->getSingleScalarResult();
        $pages = $count / $perPage;
        return (int)(fmod($pages, 1) > 0 ? $pages + 1 : $pages);
    }

    public function getTestbedsOnPage($pageNumber, $perPage = 10)
    {
        $dql = "SELECT t FROM Testbed t WHERE t NOT IN(SELECT t2 FROM Testbed t2 JOIN t2.users u WHERE u.id='".WebApp::$User->getEntity()->getId()."') ORDER BY t.id DESC";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setFirstResult(($pageNumber - 1) * $perPage);
        $query->setMaxResults($perPage);
        return $query->getResult();
    }

    public function testbedIsset($projectGuid, $testbedGuid)
    {
        $dql = "SELECT count(t.id)FROM Testbed t WHERE t.projectGuid='".$projectGuid."' AND t.guid='".$testbedGuid."'";
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->getSingleScalarResult() == 1;
    }
}
?>
