<?php
use Doctrine\ORM\EntityRepository;

class JobRepository extends EntityRepository
{
    public function getJobsOnPage($pageNumber, $perPage = 25)
    {
        $dql = "SELECT j FROM Job j JOIN j.user u WHERE u.id='".WebApp::$User->getEntity()->getId()."' ORDER BY j.id DESC";
        $query = $this->getEntityManager()->createQuery($dql);
        $query->setFirstResult(($pageNumber - 1) * $perPage);
        $query->setMaxResults($perPage);
        return $query->getResult();
    }

    public function getJobPageCount($jobsPerPage)
    {
        $dql = "SELECT count(j.id) FROM Job j JOIN j.user u WHERE u.id='".WebApp::$User->getEntity()->getId()."'";
        $query = $this->getEntityManager()->createQuery($dql);
        $jobCount = $query->getSingleScalarResult();
        $pages = $jobCount / $jobsPerPage;
        return (int)(fmod($pages, 1) > 0 ? $pages + 1 : $pages);
    }

    public function getJobsOfUsersTestbed(User $user, Testbed $testbed)
    {
        $dql = "SELECT j FROM Job j JOIN j.user u JOIN j.testbed t"
              ." WHERE u.id='".$user->getId()."' AND t.id='".$testbed->getId()."' ORDER BY j.id ASC";
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->getResult();
    }

    public function getJobCountOfUsersTestbed(User $user, Testbed $testbed)
    {
        $dql = "SELECT count(j.id) FROM Job j JOIN j.user u JOIN j.testbed t"
              ." WHERE u.id='".$user->getId()."' AND t.id='".$testbed->getId()."' ORDER BY j.id ASC";
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->getSingleScalarResult();
    }
}
?>
