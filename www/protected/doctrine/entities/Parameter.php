<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="parameters")
 **/
class Parameter
{
    public function __construct()
    {
        $this->parameterEnums = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @Column(type="string")
     **/
    protected $type = null;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @Column(type="boolean")
     **/
    protected $isVisible = true;

    public function isVisible()
    {
        return $this->isVisible;
    }

    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @OneToMany(targetEntity="ParameterEnum", mappedBy="parameter")
     **/
    protected $parameterEnums = null;

    public function addParameterEnum($parameterEnum)
    {
        if ($parameterEnum->getParameter() === null || $parameterEnum->getParameter() !== $this)
            $parameterEnum->setParameter($this);
        else
            $this->parameterEnums->add($parameterEnum);
    }

    public function getParameterEnums()
    {
        return  $this->parameterEnums;
    }

    /**
     * @Column(type="string")
     **/
    protected $title = "";

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @Column(type="string")
     **/
    protected $units = "";

    public function getUnits()
    {
        return $this->units;
    }

    public function setUnits($units)
    {
        $this->units = $units;
    }

    /**
     * @Column(type="string")
     **/
    protected $comment = "";

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @Column(type="string")
     **/
    protected $value = null;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @ManyToOne(targetEntity="Category", inversedBy="parameters")
     **/
    protected $category = null;

    public function setCategory($category)
    {
        $this->category = $category;
        $category->addParameter($this);
    }

    public function getCategory()
    {
        return $this->category;
    }
}
?>
