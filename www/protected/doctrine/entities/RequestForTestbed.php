<?php
/**
 * @Entity @Table(name="groups")
 **/
class RequestForTestbed
{
    public function __construct()
    {
    }

    /**
     * @ManyToOne(targetEntity="User", inversedBy="requestsForTestbeds")
     **/
    protected $user = null;

    public function setUser($user)
    {
        if ($this->user != $user)
        {
            $this->user = $user;
            $user->addRequestForTestbed($this);
        }
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ManyToOne(targetEntity="Testbed", inversedBy="requests")
     **/
    protected $testbed = null;

    public function setTestbed($testbed)
    {
        if ($this->testbed != $testbed)
        {
            $this->testbed = $testbed;
            $testbed->addRequest($this);
        }
    }

    public function getTestbed()
    {
        return $this->testbed;
    }
}
?>
