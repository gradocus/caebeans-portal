<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="parameterEnums")
 **/
class ParameterEnum
{
    public function __construct()
    {
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $value = null;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @ManyToOne(targetEntity="Parameter", inversedBy="parameterEnums")
     **/
    protected $parameter = null;

    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
        $parameter->addParameterEnum($this);
    }

    public function getParameter()
    {
        return $this->parameter;
    }
}
?>
