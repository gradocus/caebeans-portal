<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="testbeds")
 **/
class Testbed
{
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->requests = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->uploadedFiles = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $projectGuid;

    public function getProjectGuid()
    {
        return $this->projectGuid;
    }

    public function setProjectGuid($projectGuid)
    {
        $this->projectGuid = $projectGuid;
    }

    /**
     * @Column(type="string")
     **/
    protected $guid;

    public function getGuid()
    {
        return $this->guid;
    }

    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @Column(type="string")
     **/
    protected $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @Column(type="string")
     **/
    protected $author = "NULL";

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @Column(type="string")
     **/
    protected $version = "1.0";

    public function getVersion()
    {
        return $this->version;
    }

    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @Column(type="string")
     **/
    protected $state = "HIDDEN";

    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @OneToMany(targetEntity="RequestForTestbed" , mappedBy="testbed")
     **/
    protected $requests = null;

    public function addRequest($request)
    {
        if (!$this->requests->contains($request))
        {
            $this->requests->add($request);
            $request->setTestbed($this);
        }
    }

    public function getRequests()
    {
        return $this->requests;
    }

    public function removeRequest($request)
    {
        if ($this->requests->contains($request))
        {
            $this->requests->removeElement($request);
        }
    }

    /**
     * @ManyToMany(targetEntity="User")
     **/
    protected $users = null;

    public function AddUser($user)
    {
        if (!$this->users->contains($user))
        {
            $this->users->add($user);
            $user->addTestbed($this);
        }
    }

    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @OneToMany(targetEntity="UploadedFile", mappedBy="testbed")
     **/
    protected $uploadedFiles = null;

    public function addUploadedFile($uploadedFile)
    {
        if (!$this->uploadedFiles->contains($uploadedFile))
        {
            $this->uploadedFiles->add($uploadedFile);
            $uploadedFile->setTestbed($this);
        }
    }

    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    public function removeUploadedFile($uploadedFile)
    {
        if ($this->uploadedFiles->contains($uploadedFile))
        {
            $this->uploadedFiles->removeElement($uploadedFile);
        }
    }

    /**
     * @OneToMany(targetEntity="Job", mappedBy="testbed")
     **/
    protected $jobs = null;

    public function addJob($job)
    {
        if ($job->getTestbed() === null || $job->getTestbed() !== $this)
            $job->setTestbed($this);
        else
            $this->jobs->add($job);
    }

    public function getJobs()
    {
        return  $this->jobs;
    }

    /**
     * @OneToMany(targetEntity="Category", mappedBy="testbed")
     **/
    protected $categories = null;

    public function addCategory($category)
    {
        if ($category->getTestbed() === null || $category->getTestbed() !== $this)
            $category->setTestbed($this);
        else
            $this->categories->add($category);
    }

    public function getCategories()
    {
        return  $this->categories;
    }
}
?>
