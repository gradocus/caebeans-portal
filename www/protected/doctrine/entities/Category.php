<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="categories")
 **/
class Category
{
    public function __construct()
    {
        $this->parameters = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @Column(type="string")
     **/
    protected $title = "";

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @Column(type="boolean")
     **/
    protected $isVisible = true;

    public function isVisible()
    {
        return $this->isVisible;
    }

    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;
    }

    /**
     * @ManyToOne(targetEntity="Testbed", inversedBy="categories")
     **/
    protected $testbed = null;

    public function setTestbed($testbed)
    {
        $this->testbed = $testbed;
        $testbed->addCategory($this);
    }

    public function getTestbed()
    {
        return $this->testbed;
    }

    /**
     * @OneToMany(targetEntity="Parameter", mappedBy="category")
     **/
    protected $parameters = null;

    public function addParameter($parameter)
    {
        if ($parameter->getCategory() === null || $parameter->getCategory() !== $this)
            $parameter->setCategory($this);
        else
            $this->parameters->add($parameter);
    }

    public function getParameters()
    {
        return  $this->parameters;
    }
}
?>
