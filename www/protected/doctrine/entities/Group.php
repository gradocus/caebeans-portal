<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="groups")
 **/
class Group
{
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     * @var int
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     * @var string
     **/
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @ManyToMany(targetEntity="User")
     **/
    protected $users = null;

    public function addUser($user)
    {
        if (!$this->users->contains($user))
        {
            $this->users->add($user);
            $user->AddGroup($this);
        }
    }

    public function getUsers()
    {
        return  $this->users;
    }
}
?>
