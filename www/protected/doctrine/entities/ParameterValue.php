<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="parameterValues")
 **/
class ParameterValue
{
    public function __construct()
    {
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $value = null;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @ManyToOne(targetEntity="Job", inversedBy="parameterValues")
     **/
    protected $job = null;

    public function getJob()
    {
        return $this->job;
    }

    public function setJob($job)
    {
        $this->job = $job;
        $job->addParameterValue($this);
    }

    /**
     * @OneToOne(targetEntity="Parameter")
     * @JoinColumn(name="parameter_id", referencedColumnName="id")
     **/
    protected $parameter = null;

    public function getParameter()
    {
        return $this->parameter;
    }

    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }
}
?>
