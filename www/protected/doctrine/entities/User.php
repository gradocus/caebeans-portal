<?php
defined('DOCTRINE') or die("403");

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="users")
 **/
class User
{
    public function __construct()
    {
        $this->groups = new ArrayCollection();
        $this->uploadedFiles = new ArrayCollection();
        $this->requestsForTestbeds = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @Column(type="string")
     **/
    protected $email;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @Column(type="string")
     **/
    protected $company;

    public function getCompany()
    {
        return $this->company;
    }

    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @Column(type="string")
     **/
    protected $password = "";

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @Column(type="string")
     **/
    protected $firstName = "";

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @Column(type="string")
     **/
    protected $middleName = "";

    public function getMiddleName()
    {
        return $this->middleName;
    }

    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @Column(type="string")
     **/
    protected $secondName = "";

    public function getLastName()
    {
        return $this->secondName;
    }

    public function setLastName($secondName)
    {
        $this->secondName = $secondName;
    }

    /**
     * @Column(type="boolean")
     **/
    protected $isBanned = true;

    public function isBanned()
    {
        return $this->isBanned;
    }

    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;
    }

    /**
     * @ManyToMany(targetEntity="Group")
     **/
    protected $groups = null;

    public function AddGroup($group)
    {
        if (!$this->groups->contains($group))
        {
            $this->groups->add($group);
            $group->addUser($this);
        }
    }

    public function getGroups()
    {
        return $this->groups;
    }

// ----------------------------------------------------------------------------

    /**
     * @OneToMany(targetEntity="UploadedFile", mappedBy="user")
     **/
    protected $uploadedFiles = null;

    public function addUploadedFile($uploadedFile)
    {
        if (!$this->uploadedFiles->contains($uploadedFile))
        {
            $this->uploadedFiles->add($uploadedFile);
            $uploadedFile->setUser($this);
        }
    }

    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    public function removeUploadedFile($uploadedFile)
    {
        if ($this->uploadedFiles->contains($uploadedFile))
        {
            $this->uploadedFiles->removeElement($uploadedFile);
        }
    }

    /**
     * @OneToMany(targetEntity="RequestForTestbed", mappedBy="user")
     **/
    protected $requestsForTestbeds = null;

    public function addRequestForTestbed($requestForTestbed)
    {
        if (!$this->requestsForTestbeds->contains($requestForTestbed))
        {
            $this->requestsForTestbeds->add($requestForTestbed);
            $requestForTestbed->setUser($this);
        }
    }

    public function getRequestsForTestbeds()
    {
        return $this->requestsForTestbeds;
    }

    public function removeRequestForTestbed(RequestForTestbed $requestForTestbed)
    {
        if ($this->requestsForTestbeds->contains($requestForTestbed))
        {
            $this->requestsForTestbeds->removeElement($requestForTestbed);
        }
    }

    /**
     * @ManyToMany(targetEntity="Testbed")
     **/
    protected $testbeds = null;

    public function AddTestbed($testbed)
    {
        if (!$this->testbeds->contains($testbed))
        {
            $this->testbeds->add($testbed);
            $testbed->addUser($this);
        }
    }

    public function getTestbeds()
    {
        return $this->testbeds;
    }

    /**
     * @OneToMany(targetEntity="Job", mappedBy="user")
     **/
    protected $jobs = null;

    public function addJob($job)
    {
        if ($job->getUser() === null || $job->getUser() !== $this)
            $job->setUser($this);
        else
            $this->jobs->add($job);
    }

    public function getJobs()
    {
        return  $this->jobs;
    }
}
?>
