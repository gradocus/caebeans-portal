<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="uploadedFile")
 **/
class UploadedFile
{
    public function __construct()
    {
    }

    /**
     * @ManyToOne(targetEntity="User", inversedBy="files")
     **/
    protected $user;

    public function setUser($user)
    {
        if ($this->user != $user)
        {
            $this->user = $user;
            $user->addUploadedFile($this);
        }
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ManyToOne(targetEntity="Testbed", inversedBy="files")
     **/
    protected $testbed = null;

    public function setTestbed($testbed)
    {
        if ($this->testbed != $testbed)
        {
            $this->testbed = $testbed;
            $testbed->addUploadedFile($this);
        }
    }

    public function getTestbed()
    {
        return $this->testbed;
    }

    /**
     * @Column(type="string")
     **/
    protected $userFileName = null;

    public function getUserFileName()
    {
        return $this->userFileName;
    }

    public function setUserFileName($userFileName)
    {
        $this->userFileName = $userFileName;
    }

    /**
     * @Column(type="string")
     **/
    protected $fileName = null;

    public function getFileName()
    {
        return $this->fileName;
    }

    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}
?>
