<?php
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="jobs")
 **/
class Job
{
    public function __construct()
    {
        $this->parameterValues = new ArrayCollection();
    }

    /**
     * @Id @GeneratedValue @Column(type="integer")
     **/
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @Column(type="string")
     **/
    protected $guid = null;

    public function getGuid()
    {
        return $this->guid;
    }

    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * @Column(type="string")
     **/
    protected $name = null;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @ManyToOne(targetEntity="Testbed", inversedBy="jobs")
     **/
    protected $testbed = null;

    public function setTestbed($testbed)
    {
        $this->testbed = $testbed;
        $testbed->addJob($this);
    }

    public function getTestbed()
    {
        return $this->testbed;
    }

    /**
     * @ManyToOne(targetEntity="User", inversedBy="jobs")
     **/
    protected $user = null;

    public function setUser($user)
    {
        $this->user = $user;
        $user->addJob($this);
    }

    public function getUser()
    {
        return $this->user;
    }

    /**
     * @Column(type="datetime")
     **/
    protected $startTime = null;

    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    public function getStartTime()
    {
        $this->startTime->setTimezone(new DateTimeZone('YEKT'));
        return $this->startTime;
    }

    /**
     * @Column(type="datetime")
     **/
    protected $completeTime = null;

    public function setCompleteTime($completeTime)
    {
        $this->completeTime = $completeTime;
    }

    public function getCompleteTime()
    {
        $this->completeTime->setTimezone(new DateTimeZone('YEKT'));
        return $this->completeTime;
    }

    /**
     * @Column(type="string")
     **/
    protected $state = "NOT_STARTED";

    public function setState($state)
    {
        $statuses = array('NOT_STARTED', 'RUNNING', 'HELD', 'SUCCESSFULL', 'FAILED');
        if (in_array($state, $statuses))
            $this->state = $state;
        else
            throw new Exception("Can't set state for job '".$this->id."' to '".$state."'");
    }

    public function getState()
    {
        return $this->state;
    }

    /**
     * @OneToMany(targetEntity="ParameterValue", mappedBy="job")
     **/
    protected $parameterValues = null;

    public function addParameterValue($parameterValue)
    {
        if ($parameterValue->getJob() === null || $parameterValue->getJob() !== $this)
            $parameterValue->setJob($this);
        else
            $this->parameterValues->add($parameterValue);
    }

    public function getParameterValues()
    {
        return $this->parameterValues;
    }

    public function getStruct()
    {
        $testbed = $this->getTestbed();
        $resultStruct = array(
                'name' => $testbed->getName(),
                'author' => $testbed->getAuthor(),
                'version' => $testbed->getVersion(),
                'caebeanId' => $testbed->getGuid()
            );

        $parametersArray = array();
        $parameter_counter = 0;



        foreach ($testbed->getCategories() as $category)
        {
/*            echo $category->getName(), ":<br>";
            $parametersArray = array();
            $parameter_counter = 0;*/
            foreach ($category->getParameters() as $parameter)
            {
                $valueOfParameter = $parameter->getValue();
                foreach ($this->parameterValues as $parameterValue)
                    if ($parameterValue->getParameter() == $parameter)
                        $valueOfParameter = $parameterValue->getValue();
                $parametersArray[$parameter_counter] = array(
                        'name' => $parameter->getName(),
                        'type' => $parameter->getType(),
                        'visible' => $parameter->isVisible(),
                        'text' => array('data' => $parameter->getTitle()),
                        'units' => array('data' => $parameter->getUnits()),
                        'comment' => array('data' => $parameter->getComment()),
                        'enums' => "",
                        'default' => $parameter->getValue(),
                        'value' => $valueOfParameter,
                    );
                $parameter_counter++;
            }

            $resultStruct['categories'] = array(
                    'category' => array(
                        'name' => $category->getTitle(),
                        'data' => $category->getName(),
                        'parameter' => $parametersArray,
                    ),
                );
        }

        if (!isset($resultStruct['categories']))
        {
            $resultStruct['categories'] = array(
                    'category' => array(
                        'name' => "temp", 
                        'data' => "temp",
                        'parameter' => array(
                            0 => array(
                                'name' => "temp",
                                'type' => "Integer",
                                'visible' => "False",
                                'text' => array('data' => ""),
                                'units' => array('data' => ""),
                                'comment' => array('data' => ""),
                                'enums' => "",
                                'default' => 0,
                                'value' => 0
                            )
                        )
                    )
                );
        }
/*
        $resultStruct['categories']['category'] = array(
                'name' => "Все в одной",
                'data' => "allInOne",
                'parameter' => $parametersArray,
            );
*/
        $resultStruct['resources'] = array();
        $resultStruct['licenses'] = array();

        return $resultStruct;
    }
}
?>
