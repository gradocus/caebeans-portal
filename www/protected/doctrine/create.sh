#!/bin/bash
SCRIPTPATH=`dirname ${BASH_SOURCE[0]}`
cd $SCRIPTPATH

mkdir proxies
chmod -R a+rw proxies

mkdir sqlite
chmod -R a+rw sqlite

../../../composer/vendor/bin/doctrine orm:schema-tool:create
chmod a+rw sqlite/*.sqlite
