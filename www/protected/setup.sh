#!/bin/bash
SCRIPTPATH=`dirname ${BASH_SOURCE[0]}`
cd $SCRIPTPATH

touch debug.log
touch error.log
chmod a+w ./*.log

mkdir uploadedFiles
chmod -R a+w uploadedFiles

cd doctrine
./create.sh
