<?php
defined("WEB_APP") or die("403");

ini_set("soap.wsdl_cache_enabled", 0);
ini_set("default_socket_timeout", 5);

class CAEServerClient
{
    public static function CreateInstance($projectID)
    {
        try
        {
            $caeServerInstance = self::GetCAEServerInstance();
            $createInstanceResponse = $caeServerInstance->createInstance(array("arg0" => $projectID));
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::CreateInstance fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeServerInstance->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeServerInstance->__getLastRequest()
                                   ."\nResponse Headers: ".$caeServerInstance->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeServerInstance->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServerClient error: ".$ex->GetMessage());
        }

        return $createInstanceResponse->return->instance->id;
    }

    public static function GetProjectsIDs()
    {
        try
        {
            $caeServerInstance = self::GetCAEServerInstance();
            $getProjectsIDsResponse = $caeServerInstance->getProjectsIDs();
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::CreateInstance fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeServerInstance->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeServerInstance->__getLastRequest()
                                   ."\nResponse Headers: ".$caeServerInstance->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeServerInstance->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServerClient error: ".$ex->GetMessage());
        }

        return $getProjectsIDsResponse->return;
    }

    public static function GetProblemCaebeans($projectID)
    {
        try
        {
            $caeServerInstance = self::GetCAEServerInstance();
            $getProblemCaebeanResponse = $caeServerInstance->getProblemCaebeans(array("arg0" => $projectID));
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::CreateInstance fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeServerInstance->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeServerInstance->__getLastRequest()
                                   ."\nResponse Headers: ".$caeServerInstance->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeServerInstance->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServerClient error: ".$ex->GetMessage());
        }

        return $getProblemCaebeanResponse->return;
    }

    public static function GetProblemCaebeanByID($projectId, $caebeanId)
    {
        try
        {
            $caeServerInstance = self::GetCAEServerInstance();
            $getProblemCaebeanByIDResponse = $caeServerInstance->getProblemCaebeanByID(array("arg0" => $projectId, "arg1" => $caebeanId));
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::CreateInstance fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeServerInstance->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeServerInstance->__getLastRequest()
                                   ."\nResponse Headers: ".$caeServerInstance->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeServerInstance->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServerClient error: ".$ex->GetMessage());
        }

        return $getProblemCaebeanByIDResponse->return;
    }

    public static function IndexAllProjects()
    {
        try
        {
            $caeServerInstance = self::GetCAEServerInstance();
            $indexAllProjectsResponse = $caeServerInstance->indexAllProjects();
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::CreateInstance fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeServerInstance->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeServerInstance->__getLastRequest()
                                   ."\nResponse Headers: ".$caeServerInstance->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeServerInstance->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServerClient error: ".$ex->GetMessage());
        }
    }

    private static $caeServerInstance = null;
    private static function GetCAEServerInstance()
    {
        if (self::$caeServerInstance === null)
            self::$caeServerInstance = @new SoapClient(
                "http://".WebApp::$Properties['caeserver']['address'].":".
                WebApp::$Properties['caeserver']['port']."/caeserver?wsdl",
                array
                    (
                        'connection_timeout' => 5,
                        'trace'              => 1,
                        'exception'          => 1
                    )
                );
        return self::$caeServerInstance;
    }
}
?>
