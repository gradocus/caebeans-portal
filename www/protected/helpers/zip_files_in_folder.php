<?php

function zip_files_in_folder($sourceFolder)
{
    if (!extension_loaded('zip') || !file_exists($sourceFolder) || !is_dir($sourceFolder))
    {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($sourceFolder.".zip", ZIPARCHIVE::CREATE))
    {
        return false;
    }

    $sourceFolder = str_replace('\\', '/', realpath($sourceFolder));
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($sourceFolder), RecursiveIteratorIterator::SELF_FIRST);

    foreach ($files as $file)
    {
        $file = str_replace('\\', '/', $file);

        if (in_array(substr($file, strrpos($file, '/')+1), array('.', '..')))
            continue;

        $file = realpath($file);

        if (is_dir($file) === true)
        {
            $zip->addEmptyDir(str_replace($sourceFolder.'/', '', $file.'/'));
        }
        else if (is_file($file) === true)
        {
            $zip->addFromString(str_replace($sourceFolder.'/', '', $file), file_get_contents($file));
        }
    }

    return $zip->close();
}

?>
