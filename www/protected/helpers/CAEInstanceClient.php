<?php
defined("WEB_APP") or die("403");

ini_set("soap.wsdl_cache_enabled", 0);
ini_set("default_socket_timeout", 60);

class CAEInstanceClient
{
    public static function SubmitJob(Job $job, $cpuCount = 1, $memorySize = 256)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $submitJobResponse = $caeInstanceClient->submitJob(
                array
                    (
                        "arg0" => array
                            (
                                "instance" => array
                                    (
                                        "id" => $job->getGuid()
                                    ),
                                "problemCaebean" => $job->GetStruct()
                            ),
                        "arg1" => $cpuCount,
                        "arg2" => $memorySize
                    )
                );
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer::SubmitJob error (".$ex->getCode()."): ".$ex->GetMessage());
        }
    }

    public static function GetStatus($instanceGuid)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $getStatusResponse = $caeInstanceClient->getStatus(
                array
                    (
                        "arg0" => array
                            (
                                "instance" => array
                                    (
                                        "id" => $instanceGuid
                                    )
                            )
                    )
                );
            $statusArray = (array)$getStatusResponse->return->Status;
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }

        $statuses = array('NOT_STARTED', 'RUNNING', 'HELD', 'SUCCESSFULL', 'FAILED');
        for ($i = 0; $i < count($statuses); $i++) {
            if (array_key_exists($statuses[$i], $statusArray))
                return $statuses[$i];
        }

        throw new Exception("CAEServer error: Unknown status \"".print_r($statusArray, true)."\"");
    }

    public static function UploadUpdates($instanceGuid, $fileContent)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $uploadUpdatesResponse = $caeInstanceClient->uploadUpdates(
                array
                    (
                        "arg0" => $instanceGuid,
                        "arg1" => $fileContent
                    )
                );
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }
    }

    public static function GetExecTime($instanceGuid)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $getExecTimeResponse = $caeInstanceClient->getExecTime(
                array
                    (
                        "arg0" => array
                            (
                                "id" => $instanceGuid
                            )
                    )
                );
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }
        if ($getExecTimeResponse->return != -1)
            return $getExecTimeResponse->return / 1000;
        else
            return $getExecTimeResponse->return;
    }

    public static function GetResultsArch($instanceGuid)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $getResultsArchResponse = $caeInstanceClient->getResultsArch(
                array
                    (
                        "arg0" => array
                            (
                                "id" => $instanceGuid
                            )
                    )
                );
            return $getResultsArchResponse->return;
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }
    }

    public static function RemoveResultDir($instanceGuid)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $caeInstanceClient->removeResultDir(
                array
                    (
                        "arg0" => array
                            (
                                "id" => $instanceGuid
                            )
                    )
                );
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }
    }

    public static function GetWorkArch($instanceGuid)
    {
        try
        {
            $caeInstanceClient = self::GetCAEClientInstance();
            $getWorkArchResponse = $caeInstanceClient->getWorkArch(
                array
                    (
                        "arg0" => array
                            (
                                "id" => $instanceGuid
                            )
                    )
                );
           return $getWorkArchResponse->return;
        }
        catch(SoapFault $fault)
        {
            throw new Exception("CAEServer::GetResultsArch fault (".$fault->faultcode."): "
                                   .$fault->faultstring."\n".$fault->getTraceAsString()
                                   ."\nRequest Headers: ".$caeInstanceClient->__getLastRequestHeaders()
                                   ."\nRequest: ".$caeInstanceClient->__getLastRequest()
                                   ."\nResponse Headers: ".$caeInstanceClient->__getLastResponseHeaders()
                                   ."\nResponse: ".$caeInstanceClient->__getLastResponse()
                               );
        }
        catch(Exception $ex)
        {
            throw new Exception("CAEServer error: ".$ex->GetMessage());
        }
        // Process response.
//        throw new Exception("NotImplementedException: CAEServer->GetWorkArch");
    }

    private static $caeClientInstance = null;
    private static function GetCAEClientInstance()
    {
        if (self::$caeClientInstance === null)
            self::$caeClientInstance = @new SoapClient(
                "http://".WebApp::$Properties['caeserver']['address'].":".
                WebApp::$Properties['caeserver']['port']."/caeinstance?wsdl",
                array
                    (
                        'connection_timeout' => 60,
                        'trace'              => 1,
                        'exception'          => 1
                    )
                );
        return self::$caeClientInstance;
    }
}
?>
