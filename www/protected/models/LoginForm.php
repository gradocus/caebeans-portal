<?php
//require_once WEB_APP_PATH."base/Model.php";

class LoginForm// extends Model
{
    public function __construct()
    {
//        parent::__construct();

        $this->login = "";
        $this->password = "";
        $this->rememberMe = false;
        $this->message = "";
    }

    public $login;
    public $password;
    public $rememberMe;
    public $message;

    protected function validate()
    {
        if ($this->login === "" && $this->password === "")
            $this->validationErrors[] = "Введите данные для авторизации.";

        if (count($this->validationErrors) === 0)
        {
            if ($this->login === "")
                $this->validationErrors['login'] = "Введите логин.";
            if ($this->password === "")
                $this->validationErrors['password'] = "Введите пароль.";
            if ($this->login === "" || $this->password === "")
                $this->validationErrors[] = "Введены не все обязательные параметры.";
        }
    }

    public function save()
    {
        throw new Exception("Unable to save login form data.");
    }
}
?>
