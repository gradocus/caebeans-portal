<?php
class UserPreferencesForm
{
    public function __construct()
    {
        $this->userName = "";
        $this->email = "";
        $this->password = "";
        $this->newPassword1 = "";
        $this->newPassword2 = "";
    }

    public $userName;
    public $email;
    public $password;
    public $newPassword1;
    public $newPassword2;

    protected function validate()
    {
        if ($this->login === "" && $this->password === "")
            $this->validationErrors[] = "Введите данные для авторизации.";

        if (count($this->validationErrors) === 0)
        {
            if ($this->login === "")
                $this->validationErrors['login'] = "Введите логин.";
            if ($this->password === "")
                $this->validationErrors['password'] = "Введите пароль.";
            if ($this->login === "" || $this->password === "")
                $this->validationErrors[] = "Введены не все обязательные параметры.";
        }
    }

    public function save()
    {
        throw new Exception("Unable to save login form data.");
    }
}
?>
