# DiVTB Portal #

## Installation ##

### Automatic ###

Устанавливается со всеми зависимостями на "чистую" OpenSUSE 12.2:

* Склонировать [DiVTB Control System](https://bitbucket.org/gradocus/divtb-control-system):

		$ git clone https://bitbucket.org/gradocus/divtb-control-system.git

* Задать параметры системы DiVTB в `divtb.cfg`

* Запустить автоматическую установку портала:

		./divtb.sh portal install

### Manually ###

Установка портала (зависимости устанавляваются вручную):

* Склонировать DiVTB Portal:

        $ git clone ssh://git@bitbucket.org/gradocus/caebeans-portal

* Установить [Doctrine ORM](http://www.doctrine-project.org/):

        $ ./doctrine-setup.sh

* Создать базу данных и сопутствующие файлы:

        $ cd www/protected
        $ ./setup.sh

 * Разместить содержимое каталога `www` в соответствующей директории вашего Web-сервера.

 * Открыть в браузере страницу: [http://localhost/install/](http://localhost/install/)

 * В случае необходимости установки тестовых стендов, открыть страницы:

        http://localhost/install/fcp/
        http://localhost/install/big_file/
        http://localhost/install/sleep/
